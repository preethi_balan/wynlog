/*
 * Javascript apply for all page (include in layout file app.blade.php)
 */

$(function () {
  // To make Pace works on Ajax calls
  $(document).ajaxStart(function() { Pace.restart(); });

  // Add csrf token to ajax call
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // Change datatable default pagination style, language
  // Eg. Change 'Previous' text => icon
  $.extend(true, $.fn.DataTable.defaults, {
    language: {
      paginate: {
        first: "<span class='glyphicon glyphicon-step-fast-backward'></span>",
        previous: "<span class='glyphicon glyphicon-step-backward'></span>",
        next: "<span class='glyphicon glyphicon-step-forward'></span>",
        last: "<span class='glyphicon glyphicon-step-fast-backward'></span>"
      },
      info: "_START_ - _END_ of _TOTAL_ entries",
      infoEmpty: "0 entries",
    }
  });

  //USAGE: $("#form").serializefiles();
  (function($) {
    $.fn.serializefiles = function() {
      var obj = $(this);
      /* ADD FILE TO PARAM AJAX */
      var formData = new FormData();
      $.each($(obj).find("input[type='file']"), function(i, tag) {
        $.each($(tag)[0].files, function(i, file) {
          formData.append(tag.name, file);
        });
      });
      var params = $(obj).serializeArray();
      $.each(params, function (i, val) {
        formData.append(val.name, val.value);
      });
      return formData;
    };
  })(jQuery);

});

// Show alert dialog
function showAlert(title, content, iconClass, onClose) {
  $.alert({
    title: title,
    content: content,
    backgroundDismiss: true,
    keyboardEnabled: true,
    cancelButton: false,
    confirmButton: 'OK',
    icon: iconClass,
    onClose: onClose
  });
}