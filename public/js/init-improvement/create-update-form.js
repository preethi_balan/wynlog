$(document).ready(function() {
  function loadDepartments() {
    var directorateEl = $('#directorate');
    var url = directorateEl.data('service-url');
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        directorate: directorateEl.val()
      }
    })
    .done(function(departments) {
      var departmentEl = $('#department');
      var departmentGroupEl = $('.department-group');
      var requiredLabelEl = departmentGroupEl.find('span.required');
      var departmentElHtml = '<option value="" selected disabled>Select department to take lead of and be impacted most by Initiative</option>';
      if (departments && departments.length) {
        var selectedDepartment = departmentEl.data('selected-department');
        for (var i = 0; i < departments.length; ++i) {
          departmentElHtml +=
            '<option value="' + departments[i].department + '"'
              + (selectedDepartment == departments[i].department ? ' selected>' : '>')
              + departments[i].department +
            '</option>';
        }
        departmentGroupEl.removeClass('hide');
        departmentEl.prop('required', true);
        requiredLabelEl.removeClass('hide');
      } else {
        departmentGroupEl.addClass('hide');
        departmentEl.prop('required', false);
        requiredLabelEl.addClass('hide');
      }

      departmentEl.html(departmentElHtml);
      if (!departmentEl.val()) {
        departmentEl.addClass('select-placeholder');
      } else {
        departmentEl.removeClass('select-placeholder');
      }
      loadBusinessUnits();
      // Trigger change onload, because it depends on value of ajax dropdown
      $('#classification-id').trigger('change');
    });
  }

  function loadBusinessUnits() {
    var departmentEl = $('#department');
    var url = departmentEl.data('service-url');
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        department: departmentEl.val()
      }
    })
    .done(function(businessUnits) {
      var businessUnitEl = $('#business-unit');
      var businessUnitGroupEl = $('.business-unit-group');
      //var requiredLabelEl = businessUnitGroupEl.find('span.required');
      var html = '<option value="" disabled selected>Select business unit to take lead of and be impacted most by Initiative</option>';
      if (businessUnits && businessUnits.length) {
        var selectedBusinessUnit = businessUnitEl.data('selected-business-unit');
        for (var i = 0; i < businessUnits.length; ++i) {
          html +=
            '<option value="' + businessUnits[i].group + '"'
              + (selectedBusinessUnit == businessUnits[i].group ? ' selected>' : '>')
              + businessUnits[i].group +
            '</option>';
        }

        businessUnitGroupEl.removeClass('hide');
        //businessUnitEl.prop('required', true);
        //requiredLabelEl.removeClass('hide');
      } else {
        businessUnitGroupEl.addClass('hide');
        //businessUnitEl.prop('required', false);
        //requiredLabelEl.addClass('hide');
      }

      if (aOfficerIsManagerOrDirector) {
        //businessUnitEl.prop('required', false);
        //requiredLabelEl.addClass('hide');
      }

      businessUnitEl.html(html);
      if (!businessUnitEl.val()) {
        businessUnitEl.addClass('select-placeholder');
      } else {
        businessUnitEl.removeClass('select-placeholder');
      }
    });
  }

  $('#actioning-officer').on('change', function() {
    /*$('#directorate').css('color','#555555');
    $('#department').css('color','#555555');
    $('#business-unit').css('color','#555555');*/
    var selected = $(this).find('option:selected');
    var curDirectorate = selected.data('directorate');
    var curDepartment = selected.data('department');
    var curBusinessUnit = selected.data('business-unit');
    aOfficerIsManagerOrDirector = selected.data('is-manager-or-director');

    $('#directorate')
      .val(curDirectorate)
      .trigger('change');

    $('#department').data('selected-department', curDepartment);
    $('#business-unit').data('selected-business-unit', curBusinessUnit);
  });

  //$('#directorate').on('change', loadDepartments);
  $('#directorate').on('change', function() {
    /*$(this).css('color','#555555');*/
    loadDepartments();
  });
  loadDepartments();

  $('#department').on('change', function() {
    loadBusinessUnits();
    // Change list of relevant departments
    $('#classification-id').trigger('change');
  });

  // $('#service-id').select2();

  $('input.benefits').click(function() {
    var benefitsDetails = $('#' + this.name + '-details');
    var benefitsDetailsGroup = $('.' + this.name + '-details');
    if (this.value == 1) {
      benefitsDetailsGroup.removeClass('hide');
      benefitsDetails.prop('required', true);
      benefitsDetails.focus();
      if (this.name == 'risk-benefit') {
        showAlert(
          'Notice',
          'If the risks have been identified in RiskWare please reference the RiskWare ID number here.',
          'fa fa-warning text-warning'
        );
      }
    } else {
      benefitsDetailsGroup.addClass('hide');
      benefitsDetails.prop('required', false);
    }
  });

  var helpContentHtml = '';
  $('#btn-benefits-help').click(function() {
    if (!helpContentHtml) {
      helpContentHtml = '<dl>';
      $('.benefits-label').each(function(index) {
        var label = $(this).text();
        var helpContent = $('.benefits-label-help').eq(index).data('original-title');
        helpContentHtml +=
          '<dt>' + label + '</dt>' +
          '<dd>' + helpContent + '</dd>';
      });
      helpContentHtml += '</dl>';
    }
    $.alert({
      title: 'More info regarding Benefit Categories',
      content: helpContentHtml,
      columnClass: 'col-md-6 col-md-offset-3',
      backgroundDismiss: true,
      keyboardEnabled: true,
      cancelButton: false,
      confirmButton: 'OK',
      icon: 'fa fa-question-circle text-info'
    });
  });

  var listAllDepartments = [];
  function getAllDepartments() {
    if (!listAllDepartments || listAllDepartments.length == 0) {
      var url = $('#classification-id').data('all-depts-url');
      return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json'
      })
      .done(function(depts) {
        listAllDepartments = depts;
      });
    }
  }

  $('#classification-id').change(function() {
    var crossDepartment = $('#cross-department');
    var crossDepartmentGroup = $('.cross-department');
    var businessUnitEl = $('#business-unit');
    var businessUnitGroupEl = $('.business-unit-group');
    var requiredLabelEl = businessUnitGroupEl.find('span.required');
    
    // Cross-Organisational Initiative
    if (this.value == CROSS_ORGANISATIONAL_INITIATIVE) {
      //Relevant Departments
      crossDepartmentGroup.removeClass('hide');
      crossDepartment.prop('required', true);
      var selectedDepartment = $('#department').val();
      var selectedCrossDepartments = crossDepartment.data('selected-cross-department');
      var selectedCrossDepartmentsValue = [];
      if (selectedCrossDepartments) {
        for (var i = 0; i < selectedCrossDepartments.length; ++i) {
          selectedCrossDepartmentsValue.push(selectedCrossDepartments[i].Department);
        }
      }
      var html = '';
      $.when(getAllDepartments()).done(function() {
        listAllDepartments.map(function(deptObj) {
          var dept = deptObj.department;
          if (dept != selectedDepartment) {
            html += '<option value="' + dept + '"' + (selectedCrossDepartmentsValue.indexOf(dept) != -1 ? 'selected' : '') + '>' + dept + '</option>';
          }
        });

        crossDepartment.html(html);
        crossDepartment.chosen({});
        crossDepartment.trigger("chosen:updated");
      });
    } else {
      crossDepartmentGroup.addClass('hide');
      crossDepartment.prop('required', false);
    }

    //Local Workplace Initiative
    if (this.value == LOCAL_WORKPLACE_INITIATIVE) { 
      businessUnitEl.prop('required', true);
      requiredLabelEl.removeClass('hide');
    } else {
      businessUnitEl.prop('required', false);
      requiredLabelEl.addClass('hide');
    }
  });

  $('#actioning-officer').chosen({});

  $('#project-sponsor').chosen({});


  $('#start-date').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd"
  });

  var referencesGroup = $('.references-group .col-sm-9');
  $('#add-comment-ref').click(function() {
    var html = '<div class="input-group">'
                + '<span class="input-group-addon">'
                  + '<a href="javascript:void(0)" class="btn-remove-ref"><i class="fa fa-remove text-danger"></i></a>'
                + '</span>'
                + '<span class="input-group-addon">'
                  + '<i class="fa fa-comment-o"></i>'
                + '</span>'
                + '<textarea name="ref-comments[]" class="form-control" placeholder="Text (Reference ID)" rows="1" required></textarea>'
                + '<input type="text" name="ref-comments-desc[]" class="form-control" placeholder="Enter any additional details">'
              + '</div>'
              + '<span class="error-ref-comments text-danger"></span>';
    referencesGroup.append(html);
  });
  $('#add-url-ref').click(function() {
    var html = '<div class="input-group">'
                + '<span class="input-group-addon">'
                  + '<a href="javascript:void(0)" class="btn-remove-ref"><i class="fa fa-remove text-danger"></i></a>'
                + '</span>'
                + '<span class="input-group-addon">'
                  + '<i class="fa fa-external-link"></i>'
                + '</span>'
                + '<input type="url" name="ref-urls[]" class="form-control" placeholder="Website URL" required>'
                + '<input type="text" name="ref-urls-desc[]" class="form-control" placeholder="e.g. website name and/or description">'
              + '</div>'
              + '<span class="error-ref-urls text-danger"></span>';
    referencesGroup.append(html);
  });
  $('#add-file-ref').click(function() {
    var html = '<div class="input-group">'
                + '<span class="input-group-addon">'
                  + '<a href="javascript:void(0)" class="btn-remove-ref"><i class="fa fa-remove text-danger"></i></a>'
                + '</span>'
                + '<span class="input-group-addon">'
                  + '<i class="fa fa-file-text-o"></i>'
                + '</span>'
                + '<input type="file" name="ref-files[]" class="form-control" placeholder="File" required>'
                + '<input type="text" name="ref-files-desc[]" class="form-control" placeholder="e.g. file name or description">'
              + '</div>'
              + '<span class="error-ref-files text-danger"></span>';
    referencesGroup.append(html);
  });
  referencesGroup.on('click', '.btn-remove-ref', function() {
    var inputGroup = $(this).parent().parent();
    // Remove error block
    inputGroup.next().remove();
    // Remove group itself
    inputGroup.remove();
  });

  $('.btn-remove-exist-ref').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var refGroup = $(this).parent().parent().parent();
    var button = $(this);
    $.confirm({
      title: 'Confirm',
      content: 'Delete reference?',
      icon: 'fa fa-question-circle-o text-danger',
      confirmButton: 'Yes',
      cancelButton: 'Cancel',
      confirm: function(){
        $.ajax({
          url: button.attr('href'),
          type: 'post',
          dataType: 'json',
          data: {
            'ID': button.data('ref-id')
          }
        })
        .done(function(response) {
          showAlert(
            response.message,
            '',
            'fa fa-check text-success',
            function() {
              // Remove group itself
              refGroup.remove();
            }
          );
        })
        .fail(function(jqXHR) {
          if (jqXHR.status == 400 || jqXHR.status == 500) {
            showAlert(
              'Error',
              jqXHR.responseJSON.message,
              'fa fa-times text-danger'
            );
          }
        });
      }
    });
    return false;
  });

  $('[data-toggle="tooltip"]').tooltip();

  $('#phase-id').change(function() {
    var completionDate = $('#completion-date');
    var completionDateGroup = $('.completion-date');
    var comments = $('#comments');
    var commentsLabelRequired = $('label[for="comments"] .required');
    var commentsError = $('.error-comments');

    completionDate.prop('required', false);
    completionDateGroup.addClass('hide');
    comments.prop('required', false);
    commentsLabelRequired.addClass('hide');
    commentsError.text('');

    var phaseId = this.value;
    if (phaseId == PHASE_NEW || phaseId == PHASE_ONHOLD) {
      $("label[for='start-date'] .label-text").text('Proposed Start Date');
    } else {
      $("label[for='start-date'] .label-text").text('Start Date');
    }
    // Completed
    if (phaseId == PHASE_COMPLETED) {
      completionDate.prop('required', true);
      completionDateGroup.removeClass('hide');
      showAlert(
        'Notice',
        'A "Completed" Initiative becomes an Improvement and will not be editable. Please confirm that all details are correct and ensure Benefits have been updated.',
        'fa fa-warning text-warning'
      );
    } else if (phaseId == PHASE_ONHOLD || phaseId == PHASE_CANCELLED) {
      comments.prop('required', true);
      commentsLabelRequired.removeClass('hide');
      commentsError.text('Please state reasons for the Phase change');
    }
  });
  $('#phase-id').trigger('change');

  $('#completion-date').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    endDate: '+0D'
  });

  $('#none-reporting-tag').click(function() {
    if (this.checked) {
      $('.reporting-tags-inputs').prop('disabled', true);
      $('.reporting-tags-inputs').prop('checked', false);
    } else {
      $('.reporting-tags-inputs').prop('disabled', false);
    }
  });

  $('.reporting-tags-inputs').click(function () {
    if(this.checked) {
      $('#none-reporting-tag').prop('checked', false);
    };
  });

  $('#supporting-departments').chosen({});

  var initForm = $('#init-form');
  var isEditForm = initForm.data('is-edit-form');

  function setSelectPlaceholderStyle() {
    $('select').each(function () {
      if (!$(this).val()) {
        $(this).addClass('select-placeholder');
      } else {
        $(this).removeClass('select-placeholder');
      }
    });
  }
  setSelectPlaceholderStyle();

  initForm.on('change', 'select', function() {
    setSelectPlaceholderStyle();
  });

  initForm.on('change', 'input, textarea, select', function() {
    $('.error-' + this.name.replace('[]', '')).text('');
  });

  initForm.on('click', 'input, textarea, select, .chosen-container', function() {
    $(this).closest('.form-group').addClass('focused');
  });

  function setChosenSelectError() {
    $('.focused .chosen-container').prev('select').each(function () {
      if ($(this).prop('required') && !this.value) {
        $(this).closest('.form-group').addClass('has-error');
      }
    });
  }

  initForm.click(function () {
    setChosenSelectError();
  });

  initForm.validator({
    focus: false,
    custom: {
      chosenvalidate: function($el) {
        var emptyValue = $el.val() == null || $el.val() == '';
        if ($el.prop('required') && emptyValue) {
          return true;
        }
        
        return false;
      }
    }
  }).on('submit', function (e) {
    $('.form-group').addClass('focused');

    if (e.isDefaultPrevented()) {
      setChosenSelectError();
      showAlert(
        'Notice',
        'The form has errors, please check again!',
        'fa fa-warning text-warning',
        function() {
          errorEl = $('.has-error').first();
          $('html, body').animate({
            scrollTop: errorEl.offset().top - 40
          }, 1000);

          setTimeout(function() { 
            errorEl.find('input[type=text]:visible,textarea:visible,select:visible').first().focus();
          }, 1000);
        }
      );
      return false;
    }
    // At least one reporting tag is checked
    if ($('.reporting-tags-inputs:checked, #none-reporting-tag:checked').length == 0) {
      showAlert(
        'Notice',
        'Please select at least one reporting tag!',
        'fa fa-warning text-warning'
      );
      return false;
    }

    var isPhaseCompleted = this['phase-id'] && this['phase-id'].value == PHASE_COMPLETED;
    if (isPhaseCompleted) {
      if (!this['ref-comments[]'] && !this['ref-urls[]']
        && !this['ref-files[]'] && $('.had-old-ref').length == 0) {
        showAlert(
          'Notice',
          'Please add at least one Reference for Improvement!',
          'fa fa-warning text-warning'
        );
        return false;
      }

      // At least benefits checked as yes
      if ($('.benefits[value="1"]:checked').length == 0) {
        showAlert(
          'Notice',
          'Please add at least one Benefit for Improvement!',
          'fa fa-warning text-warning',
          function() {
            $('html, body').animate({
              scrollTop: 0
            }, 1000);
          }
        );
        return false;
      }
    }
    var form = $(this);
    var buttonSubmit = form.find('[type="submit"]').first();
    var loading = buttonSubmit.find('.loading').first();
    buttonSubmit.prop('disabled', true);
    loading.removeClass('hide');
    var data = form.serializefiles();
    $.ajax({
      type: 'post',
      cache: false,
      contentType: false,
      processData: false,
      data: data
    })
    .always(function() {
      buttonSubmit.prop('disabled', false);
      loading.addClass('hide');
    })
    .done(function() {
      var backUrl = form.data('back-url');
      if (isEditForm) {
        showAlert(
          'Updated successfully.',
          '',
          'fa fa-check text-success',
          function() {
            if (isPhaseCompleted) {
              location.href = form.data('view-url');
            } else {
              location.reload(true);
            }
          }
        );
      } else {
        $.confirm({
          title: 'A new Initiative was created',
          content: 'Would you like to add another?',
          icon: 'fa fa-check text-success',
          confirmButton: 'Yes',
          cancelButton: 'No, I am done',
          confirm: function() {
            location.reload(true);
          },
          cancel: function() {
            location.href = backUrl;
          }
        });
      }
    })
    .fail(function(jqXHR) {
      if (jqXHR.status == 400) {
        showAlert(
          'Error',
          jqXHR.responseJSON.error_message,
          'fa fa-times text-danger'
        );
      }
      // Laravel validation status
      else if (jqXHR.status == 422) {
        showAlert(
          'Error',
          'The form have errors, please update your input!',
          'fa fa-times text-danger'
        );
        var messages = jqXHR.responseJSON;
        var keys = Object.keys(messages);
        for (var i = 0; i < keys.length; ++i) {
          var key = keys[i];
          var message = messages[key];
          var regex = new RegExp('.\\d+$');
          if (regex.test(key)) {
            var lastDot = key.lastIndexOf('.');
            var index = key.substr(lastDot + 1);
            var field = key.substr(0, lastDot);
            var fieldErrorEl = $('.error-' + field).eq(index);
            if (fieldErrorEl.parent().hasClass('input-group')) {
              fieldErrorEl = fieldErrorEl.parent();
            }
          } else {
            var fieldErrorEl = $('.error-' + key);
          }
          if (fieldErrorEl) {
            fieldErrorEl.text(message);
          }
        }
      }
    });
    return false;
  });

  $(document).ajaxStop(function () {
    initForm.validator('update');
    var scrollBarOpts = {
      theme:"dark-3",
      scrollButtons: {
        enable: true,
        scrollAmount: 10
      },
    };
    $(".box-benefits-body").css('max-height', '600px');
    $(".box-benefits-body").mCustomScrollbar(scrollBarOpts);

    $(".box-additional-info-body").css('max-height', ($('.box-initiative-body')[0].clientHeight + $('.box-reporting-tags-body')[0].clientHeight - $(".box-benefits-body")[0].clientHeight) + 'px');
    $(".box-additional-info-body").mCustomScrollbar(scrollBarOpts);

    $(".box-audit-trail-body").css('max-height', '300px');
    $(".box-audit-trail-body").mCustomScrollbar(scrollBarOpts);
  });
});
