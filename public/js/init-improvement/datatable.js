$(function () {
  var dataTable = $('.datatable').DataTable({
    buttons: [
      //{
        //text: '<i class="fa fa-plus-square" aria-hidden="true"></i> Add New Initiative',
        //className: 'btn btn-primary btn-main btn-red'
      //},
      {
        extend: 'excel',
        text: '<i class="fa fa-file-excel-o"></i> Export to Excel',
        className: 'btn btn-default btn-blue',
        exportOptions: {
          // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
          // Colums selector as jQuery https://datatables.net/reference/type/column-selector#jQuery
          columns: $('.export-table thead th:not(.no-export)').sort(function (colA, colB) {
            var colAOrder = parseInt($(colA).data('export-col-order'), 10);
            var colBOrder = parseInt($(colB).data('export-col-order'), 10);

            if (colAOrder < colBOrder) {
              return -1;
            }

            if (colAOrder > colBOrder) {
              return 1;
            }

            return 0;
          })
        }
      },
      {
        extend: 'colvis',
        text: '<i class="fa fa-eye-slash"></i> Show/Hide Columns',
        className: 'btn btn-default btn-blue',
        columns: ':gt(0)'
      },
    ],
    columns: [
      null,                 //id            --
      null,                 //title         --
      { "visible": false }, //date created  --
      { "visible": false }, //created by    --    
      { "visible": false }, //username      --
      { "visible": false }, //desc          --
      { "visible": false }, //dir           sponsor
      { "visible": false }, //dept          dir
      null,                 //unit          dept
      null,                 //classification unit
      { "visible": false }, //other dept    phase
      null,                 //actioning     --
      null,                 //start date    classification
      { "visible": false }, //completed     other dept 
      null,                 //phase         start date
      { "visible": false }, //supporting    --
      //null, //services
      { "visible": false }, //stakeholders  --
      { "visible": false }, //sponsor       last updated
      { "visible": false }, //confidential  --
      { "visible": false }, //benefits      --
      { "visible": false },
      { "visible": false },
      { "visible": false },
      { "visible": false },
      { "visible": false },
      { "visible": false }, //tags          --
      { "visible": false }, //ref           --
      { "visible": false }, //comments      --
      null,                 //last updated  completed date
      null                 //action         --
    ] ,
    order: [],
    dom: "<'row'<'col-md-3'l><'col-md-6 text-right'B><'col-md-3'f>>" +
           "<'row'<'col-md-12'tr>>" +
           "<'row'<'col-md-5'i><'col-md-7'p>>",
    drawCallback: function(settings) {
      if (!$('.datatable').parent().hasClass('table-responsive')) {
        $('.datatable').wrap("<div class='table-responsive'></div>");
      }
    }
  });


  dataTable.columns().every(function() {
    var column = this;

    $('.filter-column', this.footer()).on('keyup change', function() {
      if (column.search() !== this.value) {
        column
          .search(this.value)
          .draw();
        this.focus();
      }
    });
  });
});
