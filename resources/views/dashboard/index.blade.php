@extends('layouts.app')

@section('css')

@endsection

@section('content-header')
Dashboard
@endsection

@section('content')
<div class="row row-eq-height">
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          User Profile - {{ Auth::user()-> name }}
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-12">
            <table class="table" id="tbProfile">
              <tr>
                <td><small class="text-uppercase">Position</small> </td>
                <td>{{ $user->positionTitle }}</td>
              </tr>
              <tr>
                <td><small class="text-uppercase">Business Unit</small> </td>
                <td>{{ $user->group }}</td>
              </tr>
              <tr>
                <td><small class="text-uppercase">Department</small> </td>
                <td>{{ $user->department }}</td>
              </tr>
              <tr>
                <td><small class="text-uppercase">Directorate</small> </td>
                <td>{{ $user->directorate }}</td>
              </tr>
            </table>
            <hr>
            <h5>Recently Added or Actioned</h5>
              <ul>
              @forelse ($recentUserInit as $init)
                <li>
                  <a href="{{ url('view-initiative', [$init]) }}">{{ $init->Title }}</a> - <small class="text-uppercase">{{ $init->ActioningOfficerUser ? $init->ActioningOfficerUser->name : $init->ActioningOfficer }}</small>
                </li>
              @empty
                <li><i>You have no associated Initiatives</i></li>
              @endforelse
              </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Overview of Activity - {{ $dashboardLabels['overview'] }}
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-7">
          <p>The chart below shows recent activity (last 12 months) for the department in the form of Initiatives and how they have progressed. </p>
          <div class="chart">
            <canvas id="init-counts-pies" height=""></canvas>
            <div class="pie-right-legends">
              <p id="pie-total"></p>
              <p id="pie-inits-active"></p>
              <p id="pie-inits-cancel"></p>
              <p id="pie-imps"></p>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <h5>Recent Initatives</h5>
            <ul>
            @forelse ($recentInits as $init)
              <li>
                <a href="{{ url('view-initiative', [$init]) }}">{{ $init->Title }}</a> - <small class="text-uppercase">{{ $init->ActioningOfficerUser ? $init->ActioningOfficerUser->name : $init->ActioningOfficer }}</small>
              </li>
            @empty
              <li><i>No new Initiatives have been added in the past 12 months</i></li>
            @endforelse
            </ul>
          <h5>Recent Improvements</h5>
            <ul>
            @forelse ($recentImps as $init)
              <li>
                <a href="{{ url('view-improvement', [$init]) }}">{{ $init->Title }}</a>
              </li>
            @empty
              <li><i>No Initiatives have been completed in the past 12 months</i></li>
            @endforelse
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row row-eq-height">
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Month View - Initiatives
        </div>
      </div>
      <div class="box-body">
        <p>The graph below displays the number of active Initiatives for each month. <i>Note, "active" Initiatives include those that are in phase New, Active or On Hold</i></p>
        <div class="chart">
          <canvas id="init-counts-months-line" height="250"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Initiatives by {{ $dashboardLabels['past_12_months_by'] }}
        </div>
      </div>
      <div class="box-body">
        <p>The chart below shows the spread of Initiatives that were raised across the {{ $dashboardLabels['past_12_months_message'] }} over the past 12 months. </p>
        <div class="chart">
          <canvas id="init-for-dept-stacked-bar" height="260"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Expected Benefits
        </div>
      </div>
      <div class="box-body">
        <p>The graph below shows the expected benefits of the {{ $dashboardLabels['benefits_of'] }} 's proposed Initiatives by type</p>
        <div class="chart">
          <canvas id="init-counts-benefits-hbar" height="260"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>  

<div class="row row-eq-height">
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Month View - Improvements 
        </div>
      </div>
      <div class="box-body">
        <p>In contrast, the graph below shows the number of Initiatives that were completed each month. <i>Note, completed Initiatives are defined as Improvements</i></p>
        <div class="chart">
          <canvas id="imp-counts-months-line" height="260"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Improvements by {{ $dashboardLabels['past_12_months_by'] }}
        </div>
      </div>
      <div class="box-body">
        <p>The chart below shows the spread of Initiatives that were completed across the {{ $dashboardLabels['past_12_months_message'] }} over the past 12 months. </p>
        <div class="chart">
          <canvas id="imp-for-dept-stacked-bar" height="260"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
          Realised Benefits
        </div>
      </div>
      <div class="box-body">
        <p>The graph below shows benefits realised of Initiatives that have been completed and are now Improvements</p>
        <div class="chart">
          <canvas id="imp-counts-benefits-hbar" height="260"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.row -->
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
<script>
  var API_URL = "{{ url('') }}" + '/dashboard';

  $(document).ready(function() {
    $.get(API_URL + '/init-counts-pies')
      .done(function(data) {
        $('#pie-total').css('font-weight', 'bold').text('Total: ' + data.total);
        $('#pie-inits-active').text(data.active + ' active');
        $('#pie-inits-cancel').text(data.cancel + ' cancelled');
        $('#pie-imps').text(data.improvements + ' improvements');
        // For a pie chart
        var ctx = document.getElementById("init-counts-pies");
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            options: {
              legend: {
                position: 'bottom'
              },
              tooltips: {
                callbacks: {
                  label: function(tooltipItem, chartData) {
                    var index = tooltipItem.index;
                    var label = data.labels[index];
                    var value = data.values[index]
                    return label + ': ' + value;
                  }
                }
              }
            },
            data: {
              labels: data.labels.map(function (label, index) {
                return label + ' (' + data.values[index] + ')';
              }),
              datasets: [{
                data: data.values,
                backgroundColor: ["#f3d56f", "#d2717a", "#9478b6", "#81c1c3", "#9dd27a"],
                hoverBackgroundColor: ["#f3d56f", "#d2717a", "#9478b6", "#81c1c3", "#9dd27a"]
              }]
            }
        });
      });

    function lineChart(canvasId, data, title, datasetLabel, color, pcolor) {
      var ctx = document.getElementById(canvasId);
      var myPieChart = new Chart(ctx, {
        type: 'line',
        options: {
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: title,
                fontSize: '16'
              }
            }],
            yAxes: [{
              ticks: {
                min: 0,
                beginAtZero: true
              }
            }]
          }
        },
        data: {
          labels: data.labels,
          datasets: [{
            data: data.values,
            label: datasetLabel,
            fill: false,
            spanGaps: false,
            lineTension: 0.01,
            pointStyle: 'rect',
            //borderColor: "rgba(0, 0, 255, 0.6)",
            //pointBorderColor: "rgba(0, 0, 255, 0.6)",
            //pointBackgroundColor: "rgba(0, 0, 255, 0.6)",
            borderColor: color,
            pointBorderColor: pcolor,
            pointBackgroundColor: pcolor,
            pointBorderWidth: 2,
            pointRadius: 3,
            pointHitRadius: 10
          }]
        }
      });
    }

    $.get(API_URL + '/init-counts-months-line')
      .done(function(data) {
        lineChart(
          'init-counts-months-line',
          data,
          'Total Initiatives last 12 months: ' + data.total,
          'Initiatives',
          '#429b7d',
          '#278263'
        );
      });

    $.get(API_URL + '/imp-counts-months-line')
      .done(function(data) {
        lineChart(
          'imp-counts-months-line',
          data,
          'Total Improvements last 12 months: ' + data.total,
          'Improvements',
          '#d87399',
          '#ba4571'
        );
      });

    // Assumed we have 3 Classifications => use 3 default colors
    // If Classifications increase over 3 => use new random colors
    var defaultInitBarColors = ['#9bd375', '#6ac671', '#51ab95'];
    var defaultImpBarColors = ['#f0f187', '#e8a97e', '#d87399'];
    function getRandomColor() {
      var color = 'rgba('
        + Math.floor(Math.random() * 255) + ', '
        + Math.floor(Math.random() * 255) + ', '
        + Math.floor(Math.random() * 255) + ', 0.8)';
      return color;
    }
    function barChart(canvasId, data, defaultColors) {
      var ctx = document.getElementById(canvasId);
      var myPieChart = new Chart(ctx, {
        type: 'bar',
        options: {
          maintainAspectRatio: false,
          legend: {
            position: 'bottom'
          },
          tooltips: {
            enabled: true,
            callbacks: {
              title: function(tooltipItems, data) {
                var idx = tooltipItems[0].index;
                return data.labels[idx];
              },
            }
          },
          scales: {
            xAxes: [{
              stacked: true,
              ticks: {
                callback: function(value) {
                  var maxLength = 13;
                  var xSubstring = value.substring(0, maxLength);
                  if(xSubstring.length < maxLength) {
                    return xSubstring;
                  }

                  return xSubstring + '...';
                },
              }
            }],
            yAxes: [{
              stacked: true,
              ticks: {
                min: 0,
                beginAtZero: true
              }
            }]
          }
        },
        data: {
          labels: data.labels,
          datasets: data.datasets.map(function(dataset, index) {
            dataset.backgroundColor = defaultColors[index] || getRandomColor();
            return dataset;
          })
        }
      });
    }
    $.get(API_URL + '/init-for-dept-stacked-bar')
      .done(function(data) {
        barChart('init-for-dept-stacked-bar', data, defaultInitBarColors);
      });

    $.get(API_URL + '/imp-for-dept-stacked-bar')
      .done(function(data) {
        barChart('imp-for-dept-stacked-bar', data, defaultImpBarColors);
      });

    function hBarChart(canvasId, data, color) {
      var ctx = document.getElementById(canvasId);
      var myPieChart = new Chart(ctx, {
        type: 'horizontalBar',
        options: {
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              ticks: {
                min: 0,
                beginAtZero: true
              }
            }]
          }
        },
        data: {
          labels: data.labels,
          datasets: [{
            data: data.values,
            backgroundColor: color
          }]
        }
      });
    }

    $.get(API_URL + '/init-counts-benefits-hbar')
      .done(function(data) {
        hBarChart('init-counts-benefits-hbar', data, '#3c8b97');
      });

    $.get(API_URL + '/imp-counts-benefits-hbar')
      .done(function(data) {
        hBarChart('imp-counts-benefits-hbar', data, '#eacb5f');
      });
  });
</script>
@endsection
