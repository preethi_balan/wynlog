<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wyn-Log - New Initiative</title>
        <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    </head>

    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: Calibri;height: 100% !important;width: 100% !important;">
      <center>
         <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;background-color: #ffffff;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
               <tr>
                  <td align="center" valign="top" style="margin: 0;padding: 20px;height: 100% !important;width: 100% !important;">

                     <!-- BEGIN TEMPLATE // -->
                     <table border="0" cellpadding="0" cellspacing="0" style="width:600px; border: 1px solid #BBBBBB;border-collapse: collapse !important;">
                           <tr>
                              <td align="center" valign="top">
                                 <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#1f3c6c; border-top:3px solid #b5121b; border-bottom:1px solid #1f3c6c; border-collapse:collapse !important;">
                                        <tr>
                                            <td valign="middle" style="color:#ffffff; padding:2px; text-align:left; vertical-align:middle; width:47px;">
                                                <a href="{{ url('/') }}" style="color:#ffffff; font-weight:normal; text-decoration:none;">
                                                   <img src="{{ $message->embed('images/wynlog-logo-33x50.png') }}" alt="Wyn-Log Initiatives & Improvements Register" border="0" style="-ms-interpolation-mode:bicubic; border:0; height:auto;line-height: 100%;outline: none;text-decoration: none;float: left;padding-left: 10px;">
                                                </a>
                                            </td>
                                            <td valign="middle" style="color:#ffffff; padding:0px 2px 4px 2px; text-align:left; vertical-align:middle; font-weight:normal;">
                                                <a href="{{ url('/') }}" style="color:#ffffff; text-decoration:none;">
                                                      <span style="font-style:normal; line-height:1.5em; letter-spacing:normal; text-align:left; display:inline-block; font-size:20px; margin:0; color:#ffffff !important; font-weight:normal; font-family:DaxOT-Medium,Source Sans Pro,Helvetica Neue,Helvetica;">Wyn-Log</span> <small style="font-size:16px; font-weight:normal; font-family:DaxOT-Regular,Source Sans Pro,Helvetica Neue,Helvetica;">Initiatives & Improvements Register</small>
                                               
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#F4F4F4; border-top:1px solid #FFFFFF; border-bottom:1px solid #CCCCCC; border-collapse:collapse !important; font-family: Calibri;">
                                        <tr>
                                            <td valign="top" mc:edit="body_content00" style="color: #494949;font-size: 14px;line-height: 150%;padding: 15px 20px;text-align: left;">
                                                <h2 style="font-style: normal;line-height: 100%;letter-spacing: normal;text-align: left;display: block;font-size: 14px;font-weight: bold;margin: 10px 0 20px 0;color: #666666 !important;"> Congratulations, the following Initiative has been added to Wyn-Log: </h2>

                                                <p>
                                                   <a href="{{ url('view-initiative', [$initiative]) }}">{{ $initiative->Title }} (#{{ $initiative->id_str }})</a>
                                                   <br>
                                                   {{ $initiative->Description }}
                                                </p>
                                                <p>
                                                   <strong>Submitted by:</strong> {{ $initiative->Name }}<br>
                                                   <strong>Actioning Officer:</strong> {{ $initiative->ActioningOfficerUser->name }} 
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #DEE0E2;border-top: 1px solid #DEE0E2;border-collapse: collapse !important;">
                                       
                                        <tr>
                                            <td valign="top" style="padding-top: 0;color: #666666;font-size: 14px;line-height: 120%;padding: 20px;text-align: center;font-style: italic;" mc:edit="footer_content02">
                                                For further tips on progressing the Initiative, visit the <a href="https://au.promapp.com/wyndham/Process/7c2b3880-9c2e-4559-9315-88ef2a520d5d" style="color: #367fa9;font-weight: normal;text-decoration: underline;">CI Process in Promapp.</a> <br>
                                                For any assistance, contact <a href="mailto:Excellence@wyndham.vic.gov.au" style="color: #367fa9;font-weight: normal;text-decoration: underline;">Excellence@wyndham.vic.gov.au</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>

