<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
  <title>Wyn-Log Initiatives & Improvements Register</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSRF token for ajax -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css">
  <!-- Datatabe -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css">
  {{-- jquery-confirm --}}
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/2.5.1/jquery-confirm.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/skins/skin-red-light.css') }}">
  {{-- Pace loading --}}
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/orange/pace-theme-flash.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker3.min.css">
  <!-- jquery-custom-content-scroller -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

  {{-- Custom CSS --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-chosen.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/equal-height-columns.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
  @yield('css')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
@if (App::environment('development')) 
<body class="hold-transition skin-red-light layout-top-nav env-dev">
@elseif (App::environment('production')) 
<body class="hold-transition skin-red-light layout-top-nav env-prod">
@elseif (App::environment('training')) 
<body class="hold-transition skin-red-light layout-top-nav env-train">
@endif

<div class="wrapper">
  @if (App::environment('training')) 
  <div class="env-banner">Training Environment</div>
  @endif

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="{{ url('/') }}" class="navbar-brand"><b>Wyn-Log</b> <span class="hidden-xs">Initiatives & Improvements Register</span></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
          @if (Auth::check())
          <ul class="nav navbar-nav">
            <li{!! Request::is('/*') ? ' class="active"' : '' !!}><a href="{{ url('/') }}">Dashboard</a></li>
            <li{!! Request::is('initiatives*') ? ' class="active"' : '' !!}><a href="{{ url('initiatives') }}">Initiatives</a></li>
            <li{!! Request::is('improvements*') ? ' class="active"' : '' !!}><a href="{{ url('improvements') }}">Improvements</a></li>
            <li{!! Request::is('about*') ? ' class="active"' : '' !!}><a href="{{ url('about') }}">About</a></li>
          </ul>
          @endif
          <!-- Navbar Right Menu -->
          <ul class="nav navbar-nav navbar-right">
            @if (Auth::check())
            <!-- User Account Menu -->
            <li class="dropdown">
              <a href="mailto:{{ $user->email }}" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->username }} <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="javascript:;">
                    <p class="h4 text-bold">{{ $user->name }}</p>
                    <p><i>{{ $user->positionTitle }}</i></p>
                    <!--<p>{{ $user->email }}</p>-->
                  </a>
                </li>
                <li class="divider"></li>
                <li class="dropdown-header text-uppercase text-bold">Stats for {{ $currentYear }} to date</li>
                <li class="no-link">
                  <a href="javascript:;">
                    <span class="badge badge-init">&nbsp;</span>
                    Initiatives &nbsp;
                    <span class="badge badge-imp">&nbsp;</span>
                    Improvements
                  </a>
                  <br>
                </li>
                <li class="dropdown-header text-uppercase no-link">Business Unit</li>
                <li class="no-link">
                  <a href="javascript:;">
                    <span class="badge badge-init">{{ $businessUnitStats['init'] }}</span>
                    <span class="badge badge-imp">{{ $businessUnitStats['imp'] }}</span>
                    {{ $user->group }}
                  </a>
                </li>
                <li class="dropdown-header text-uppercase">Department</li>
                <li class="no-link">
                  <a href="javascript:;">
                    <span class="badge badge-init">{{ $departmentStats['init'] }}</span>
                    <span class="badge badge-imp">{{ $departmentStats['imp'] }}</span>
                    {{ $user->department }}
                  </a>
                </li>
                <li class="dropdown-header text-uppercase">Directorate</li>
                <li class="no-link">
                  <a href="javascript:;" class="no-link">
                    <span class="badge badge-init">{{ $directorateStats['init'] }}</span>
                    <span class="badge badge-imp">{{ $directorateStats['imp'] }}</span>
                    {{ $user->directorate }}
                  </a>
                  <br>
                </li>
                @if (Auth::user()->isAdmin() || Auth::user()->isImpersonating())
                <li class="divider"></li>
                <li>
                  @if (Auth::user()->isImpersonating())
                  <a href="{{ url('impersonate/stop') }}"><h5 class="text-danger"><i class="fa fa-user-circle-o"></i> Stop Impersonating</h5></a>
                  @else
                  <a href="{{ url('impersonate') }}"><h5><i class="fa fa-user-circle"></i> Impersonate User</h5></a>
                  @endif
                  <br>
                </li>
                @endif
              </ul>
            </li>
            @endif
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          @yield('content-header')
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
        @yield('content')
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container-fluid">
      <div class="pull-left">
        <a href="mailto:excellence@wyndham.vic.gov.au">
          <img src="{{ asset('images/excellence-icon.png') }}">Excellence@Wyndham
        </a>
        <strong>&copy; Wyndham City Council</strong>
         <!--<ul class="nav navbar-nav navbar-left">
          <li><a href="#">User Guide</a></li>
          <li></li>
         </ul>-->
      </div>
      <div class="pull-right">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="https://objective.wyndham.vic.gov.au/id:A1479771/document/versions/published" target="_blank">User Guide</a></li>
          <li>
            @if (App::environment('training')) 
            <a href="http://civicgisapp1/Wynlog/" target="_blank">Live Environment</a>
            @else
            <a href="http://civicgisapp1/Wynlog_Training" target="_blank">Training Environment</a>
            @endif
          </li>
        </ul>
      </div>
    </div>
    <!-- /.container-fluid -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
{{-- Pace loading --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/AdminLTE.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
{{-- jquery-confirm --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/2.5.1/jquery-confirm.min.js"></script>
<!-- jquery-custom-content-scroller -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
{{-- Custom JS --}}
@yield('js')

</body>
</html>
