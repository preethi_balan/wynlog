@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.bootstrap.min.css" type="text/css" />
@endsection

@section('content-header')
Improvements
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-body">
        <table id="table-improvements" class="export-table datatable table table-hover table-bordered responsive stripe">
          <thead>
            <tr>
              <th data-export-col-order="1">ID</th>
              <th data-export-col-order="2">Initiative</th>
              <th data-export-col-order="3">Date Created</th>
              <th data-export-col-order="4">Created By</th>
              <th data-export-col-order="5">Username</th>
              <th data-export-col-order="6">Description</th>
              <th data-export-col-order="19">Sponsor</th>
              <th data-export-col-order="7">Directorate</th>
              <th data-export-col-order="8">Department</th>
              <th data-export-col-order="9">Business Unit</th>
              <th data-export-col-order="15">Phase</th>
              <th data-export-col-order="12">Actioning Officer</th>
              <th data-export-col-order="10">CI Classification</th>
              <th data-export-col-order="11">Collaborating</th>
              <th data-export-col-order="13">Start Date</th>
              <th data-export-col-order="16">Supporting</th>
              <!--<th data-export-col-order="17">Service</th>-->
              <th data-export-col-order="18">Stakeholders</th>
              <th data-export-col-order="25">Last Updated</th>
              <th data-export-col-order="20">Confidential</th>
              <th data-export-col-order="21">Customer/Community Benefits</th>
              <th data-export-col-order="22">Quality Benefits</th>
              <th data-export-col-order="23">People Benefits</th>
              <th data-export-col-order="24">Environmental Benefits</th>
              <th data-export-col-order="25">Risk Benefits</th>
              <th data-export-col-order="26">Economic Benefits</th>
              <th data-export-col-order="27">Reporting Tags</th>
              <th data-export-col-order="28">References</th>
              <th data-export-col-order="29">Comments</th>
              <th data-export-col-order="30">Complete Date</th>
              <th class="no-export" data-orderable="false">Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td>
                <select id="classification-filter" class="form-control input-sm filter-column">
                  <option value="">All</option>
                  @foreach ($classifications as $class)
                  <option value="{{ $class->Classification }}">{{ $class->Classification }}</option>
                  @endforeach
                </select>
              </td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td><input type="text" class="form-control input-sm filter-column"></td>
              <td></td>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($improvements as $init)
            @continue(Auth::user()->cannot('view-initiative', $init))
            <tr class="{{ $init->Confidential ? 'confidential-row' : '' }}">
              <td>{{ $init->id_str }}</td>
              <td>
                <i class="fa fa-{!! $init->Confidential == 1 ? 'lock' : '' !!}" aria-hidden="true"></i>
                {{ $init->Title }}
              </td>
              <td>{{ date('Y-m-d', strtotime($init->DateCreated)) }}</td>
              <td>{{ $init->Name }}</td>
              <td>{{ $init->Username }}</td>
              <td>{{ $init->Description }}</td>
              <td>{{ $init->Sponsor }}</td>
              <td>{{ $init->Directorate }}</td>
              <td>{{ $init->Department }}</td>
              <td>{{ $init->BusinessUnit ? $init->BusinessUnit : 'N/A' }}</td>
              <td>{{ $init->Phase->Phase }}</td>
              <td>{{ $init->ActioningOfficerUser ? $init->ActioningOfficerUser->name : $init->ActioningOfficer }}</td>
              <td>{{ $init->Classification->Classification }}</td>
              <td>{{ $init->getOtherDepts() }}</td>
              <td>{{ $init->StartDate }}</td>
              <td>{{ $init->getSupportDepts() }}</td>
              <!--<td>{{-- $init->ServiceCatalogue ? $init->ServiceCatalogue->Service : '' --}}</td>-->
              <td>{{ $init->Stakeholders }}</td>
              <td>{{ date('Y-m-d', strtotime($init->LastUpdated)) }}</td>
              <td>{{ $init->Confidential ? 'Yes' : 'No' }}</td>
              <!--<td>{{ $init->getBenefitsDetails() }}</td>-->
              <td>{{ $init->getServiceBenefit() }}</td>
              <td>{{ $init->getQualityBenefit() }}</td>
              <td>{{ $init->getPeopleBenefit() }}</td>
              <td>{{ $init->getEnviroBenefit() }}</td>
              <td>{{ $init->getRiskBenefit() }}</td>
              <td>{{ $init->getEcoBenefit() }}</td>
              <td>{{ $init->getReportingTags() }}</td>
              <td>{{ $init->getReferences() }}</td>
              <td>{{ $init->getComments() }}</td>
              <td>{{ $init->CompleteDate }}</td>
              <td class="text-center">
                <a href="{{ url('view-improvement', [$init]) }}" class="btn btn-default btn-sm btn-blue"> <i class="fa fa-eye"></i> </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.row -->
@endsection


@section('js')
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js"></script>
<script src="{{ asset('js/init-improvement/datatable.js') }}"></script>
@endsection