@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.bootstrap.min.css" type="text/css" />
@endsection

@section('content-header')

@endsection

@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1">

		<div class="box">
      <div class="box-header with-border">
        <div class="box-title"> 
        	Help
        </div>
      </div>
      <div class="box-body">
        <p>
          <strong>User Guide</strong><br>
          Please refer to the <a href="https://objective.wyndham.vic.gov.au/id:A1479771/document/versions/published" target="_blank">User Guide</a> for detailed instructions and general information regarding the system.<br>
      		Additionally, the process for creating and editing Initiatives can be found in <a href="https://au.promapp.com/wyndham/Process/7c2b3880-9c2e-4559-9315-88ef2a520d5d">Promapp</a><br>
   			</p>
        <p><strong>Training Sessions</strong><br>        
      		<i>CI Reporting (Wyn-Log)</i> is a one hour training session available through <a href="https://civichr2.wyndham.vic.gov.au/aurprod10/" target="_blank">My Self Service (Aurion)</a> and provides 
          hand-on exercises on how to search, enter, update and record completed Initiatives (Improvements) on Wyn-Log.
      	</p>
        <p><strong>Contact</strong><br>
      		If you have any questions or require further assistance please contact <a href="mailto:excellence@wyndham.vic.gov.au">Excellence@Wyndham</a>
        </p>
      </div>
    </div>

    <div class="box">
      <div class="box-header with-border">
        <div class="box-title">
        	Overview
        </div>
      </div>
      <div class="box-body">
				<p>
					The Wyn-Log is a centralised intranet based portal designed to capture, update and report on all Initiatives and Improvement activities undertaken across Council. <br>
					The development and implementation of the Wyn-Log is a key element of the <a href="https://objective.wyndham.vic.gov.au/id:A1363498/document/versions/published" target="_blank">Continuous Improvement Framework</a> and is aligned with the <a href="https://objective.wyndham.vic.gov.au/id:A860406/document/versions/latest" target="_blank">Australian Business Excellence Framework (ABEF)*</a>
          <br><small>* <i>Please note this is a Licenced copy – <strong>DO NOT PRINT</strong></i></small>
				</p>
        <br>
				<p>
          <strong>What is the point of the Wyn-Log?</strong><br>
					The Wyn-Log has been designed to:
				</p>
				<ul>
			    <li>Capture Ideas raised and vetted in team meetings</li>
          <li>Enable collaboration and tracking on the progress of allocated Initiatives</li> 
          <li>Encourage information sharing and collaboration between Departments</li> 
          <li>Demonstrate and promote Improvements completed by Departments</li>
          <li>Provide an information resource for staff to see what Initiatives or Improvements can be utilised by different teams</li>
          <li>Identify and nominate standardised Benefit Realisation categories and to demonstrate the benefits realised</li>
          <li>Provide a mechanism for reporting and accountability for nominated and identified Initiatives</li>
          <li>Demonstrate efficiencies and benefits to Council, the Community and the Essential Services Commission</li>
          <li>Provide evidence for Rate Capping Variation Requests, Business Plans, Risk Management Plans and PDA reporting requirements</li>
				</ul>
        <br>
        <p>
          <strong>What should I record in the Wyn-Log?</strong><br>
          All Continuous Improvement initiatives currently being undertaken, including:
        </p>
        <ul>
          <li>Initiatives from Department Plans (2016/17 and proposed for 2017/18)</li>
          <li>Initiatives linked to City Plan (2013-2017)</li>
          <li>Management Actions per RiskWare</li>
          <li>Initiatives identified as Suggested Improvements in Promapp</li>
          <li>Improvements completed in 2016 that have an economic benefit</li>
        </ul>
			</div>
    </div>

  </div>
</div>
<!-- /.row -->
@endsection


@section('js')
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
@endsection