<div class="col-md-6">
  <div class="box box-solid box-audit-trail collapsed-box">
    <div class="box-header with-border">
      <div class="box-title text-primary">Audit Trail</div>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
      </div>
    </div>
    <div class="box-body box-audit-trail-body no-padding">
      <div class="box box-widget">
        <div class="box-body box-comments box-comments-no-image">
          @foreach ($model->LogInitiatives as $log)
          <div class="box-comment">
            <div class="comment-text">
              <span class="username">
                <i class="fa fa-pencil-square-o"></i> {{ isset($log->LogActionedByUser) ? $log->LogActionedByUser->name : $log->LogActionedBy }}
                <span class="text-muted pull-right">
                  <span class="label label-default">{{ $log->Phase->Phase }}</span>
                  <span class="label label-default">{{ $log->LogActionedDate }}</span>
                </span>
              </span>
              {{ $log->LogAction }}
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.box-comment -->
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
