@extends('layouts.app')

@section('css')

@endsection

<?php
$isEditForm = $model->exists;
$isNewForm = !$isEditForm;
?>
@section('content-header')
@if ($isEditForm)
  Edit Initiative #{{ $model->id_str }}
  <div class="pull-right">
    <a class="btn btn-default btn-darkblue" target="_blank" href="{{ ($model->isImprovement() ? url('view-improvement', [$model]) : url('view-initiative', [$model])) . '?print=true' }}"><i class="fa fa-print"></i> Print</a>
  </div>
@else
  Add New Initiative
@endif
@endsection

@section('content')
@if (session()->has('error_message'))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session()->get('error_message') }}
    </div>
  </div>
</div>
@endif
<!-- form start -->
<form class="form-horizontal" id="init-form" method="post" enctype="multipart/form-data" autocomplete="off" data-back-url="{{ url('/initiatives') }}" data-view-url="{{ url('view-improvement', [$model]) }}" data-is-edit-form="{{ $isEditForm ? true : false }}" data-toggle="validator">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-solid box-initiative">
            <div class="box-header with-border">
              <div class="box-title text-primary">General Info</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body box-initiative-body">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="title" class="col-sm-3 control-label">{{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }}Title <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <input type="text" maxlength="150" name="title" class="form-control" value="{{ $model->Title }}" required placeholder="Provide a title that describes the overall aim of the Initiative" autofocus>
                  <span class="error-title text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="description" class="col-sm-3 control-label">Description <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <textarea name="description" id="description" class="form-control" rows="4" required placeholder="Provide a detailed description of the Initiative">{{ $model->Description }}</textarea>
                  <span class="error-description text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="actioning-officer" class="col-sm-3 control-label">Actioning Officer <span class="required text-danger">*</span></label>
                <div class="col-sm-9  ">
                  <select name="actioning-officer" class="form-control select2" id="actioning-officer" required data-placeholder="Select staff member responsible for actioning the Initiative">
                    <option value=""></option>
                    @foreach ($users as $item)
                    <option
                      value="{{ $item->username }}"
                      {{ $item->username == $model->ActioningOfficer ? 'selected' : '' }}
                      data-directorate="{{ $item->directorate }}"
                      data-department="{{ $item->department }}"
                      data-business-unit="{{ $item->group }}"
                      data-is-manager-or-director="{{ $item->isManagerOrDirector() ? 'true' : 'false' }}">
                      {{ $item->name }}
                      <!--{{ $item->username }} - {{ $item->name }}-->
                    </option>
                    @endforeach
                  </select>
                  <span class="error-actioning-officer text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="classification-id" class="col-sm-3 control-label">CI Classification <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="classification-id" class="form-control" id="classification-id" required data-all-depts-url="{{ url('list-all-departments') }}">
                    <option value="" disabled selected class="hide">Select the scope of the Initiative</option>
                    @foreach ($classifications as $item)
                    <option value="{{ $item->ClassID }}" {{ $item->ClassID == $model->ClassificationID ? 'selected' : '' }}>{{ $item->Classification }}</option>
                    @endforeach
                  </select>
                  <span class="error-classification-id text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="directorate" class="col-sm-3 control-label">Directorate <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="directorate" class="form-control" id="directorate" data-service-url="{{ url('list-departments') }}" required>
                    <option value="" selected disabled>Select the relevant directorate associated to the Initiative</option>
                    @foreach ($directorates as $item)
                    <option value="{{ $item->directorate }}" {{ $item->directorate == $model->Directorate && $isEditForm ? 'selected' : '' }}>{{ $item->directorate }}</option>
                    @endforeach
                  </select>
                  <span class="error-directorate text-danger"></span>
                </div>
              </div>
              <div class="form-group department-group {{ $model->Department && $isEditForm ? '' : 'hide' }}">
                <label for="department" class="col-sm-3 control-label">Department <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="department" class="form-control" id="department" data-selected-department="{{ $model->Department && $isEditForm ? $model->Department : '' }}" data-service-url="{{ url('list-business-units') }}">
                    @if ($model->Department)
                    <option value="{{ $model->Department }}" selected>{{ $model->Department }}</option>
                    @endif
                  </select>
                  <span class="error-department text-danger"></span>
                </div>
              </div>
              <div class="form-group business-unit-group {{ $model->BusinessUnit && $isEditForm ? '' : 'hide' }}">
                <label for="business-unit" class="col-sm-3 control-label">Business Unit <span class="required text-danger hide">*</span></label>
                <div class="col-sm-9">
                  <select name="business-unit" class="form-control" id="business-unit" data-selected-business-unit="{{ $model->BusinessUnit && $isEditForm ? $model->BusinessUnit : '' }}" required>
                    @if ($model->BusinessUnit)
                    <option value="{{ $model->BusinessUnit }}" selected>{{ $model->BusinessUnit }}</option>
                    @endif
                  </select>
                  <span class="error-business-unit text-danger"></span>
                </div>
              </div>
              <div class="form-group cross-department hide">
                <label for="cross-department" class="col-sm-3 control-label">Collaborating Departments <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="cross-department[]" id="cross-department" data-selected-cross-department="{{ $model->InitOtherDept }}" data-placeholder="Select any department/s actively participating in the Initiative" class="form-control select2" multiple data-chosenvalidate="true">
                    @foreach ($model->InitOtherDept as $dept)
                    <option value="{{ $dept->Department }}" selected>{{ $dept->Department }}</option>
                    @endforeach
                  </select>
                  <span class="error-cross-department text-danger"></span>
                </div>
              </div>
              <div class="form-group supporting-departments">
                <label for="supporting-departments" class="col-sm-3 control-label">Supporting Departments</span></label>
                <div class="col-sm-9">
                  <select name="supporting-departments[]" id="supporting-departments" class="form-control select2" multiple data-chosenvalidate="true" data-placeholder="Select all departments involved as a secondary/supporting role">
                    @foreach ($departments as $dept)
                    <option value="{{ $dept }}" {{ in_array($dept, $supportingDepartments) ? 'selected' : '' }}>{{ $dept }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="stakeholders" class="col-sm-3 control-label">Other Stakeholders</label>
                <div class="col-sm-9">
                  <textarea name="stakeholders" id="stakeholders" class="form-control" rows="3" placeholder="List any other relevant internal or external stakeholders">{{ $model->Stakeholders }}</textarea>
                  <span class="error-stakeholders text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="project-sponsor" class="col-sm-3 control-label">Project Sponsor <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="project-sponsor" class="form-control select2" id="project-sponsor" required data-placeholder="Select staff member responsible for overseeing the Initiative">
                    <option value=""></option>
                    @foreach ($users as $item)
                    <option value="{{ $item->name }}" {{ $item->name == $model->Sponsor ? 'selected' : '' }}>{{ $item->name }}</option>
                    @endforeach
                  </select>
                  <span class="error-project-sponsor text-danger"></span>
                </div>
              </div>
              {{--<div class="form-group">
                <label for="service-id" class="col-sm-3 control-label">Service ID <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <select name="service-id" class="form-control select2" id="service-id" required>
                    @foreach ($services as $item)
                    <option value="{{ $item->ID }}" {{ $model->ServiceID == $item->ID ? 'selected' : '' }}>{{ $item->Service }}</option>
                    @endforeach
                  </select>
                  <span class="error-service-id text-danger"></span>
                </div>
              </div>--}}
              <div class="form-group">
                <label for="start-date" class="col-sm-3 control-label">
                  <span class="label-text">Proposed Start Date</span>
                  <span class="required text-danger">*</span></label>
                <div class="col-sm-9">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="start-date" id="start-date" class="form-control pull-right" value="{{ $model->StartDate }}" required>
                    <span class="error-start-date text-danger"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="confidential" class="col-sm-3 control-label">Confidential</label>
                <div class="checkbox col-sm-9">
                  <label><input type="checkbox" name="confidential" id="confidential" value="1" {{ $model->Confidential == 1 ? 'checked' : '' }}> Hide this {{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }} from general view</label>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-12">
          <div class="box box-solid box-reporting-tags">
            <div class="box-header with-border">
              <div class="box-title text-primary">Reporting Tags <span class="required text-danger">*</span></div>
            </div>
            <div class="box-body box-reporting-tags-body">
              <div class="form-group">
                <div class="col-sm-12">
                  From the options below, please check all that are applicable, if any. Identifying the appropriate tags will assist for reporting. <br>
                  More than one may be selected. If further explanation is required, please use the <b>comments</b> field. <br><br>
                  This Initiative is/will be:<br>
                </div>
                @foreach (App\Models\InitReportingTags::columns() as $column)
                <div class="col-sm-offset-1 col-sm-11">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" class="reporting-tags-inputs" name="{{ $column['column'] }}" value="1" {{ $model->InitReportingTags && $model->InitReportingTags->{$column['column']} == 1 ? 'checked' : '' }}> <span>{!! $column['description_html'] !!}</span>
                    </label>
                  </div>
                </div>
                @endforeach
                <div class="col-sm-offset-1 col-sm-11">
                  <div class="checkbox">
                    <label>
                      <input id="none-reporting-tag" type="checkbox" name="" value="" {{ $isEditForm && !$model->getReportingTags() ? 'checked' : '' }} > <span>None of the above</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-solid box-benefits">
        <div class="box-header with-border">
          <div class="box-title text-primary">Benefit Categories <span class="required text-danger">*</span></div>
          <div class="box-tools">
            <a href="https://objective.wyndham.vic.gov.au/id:A1423264/document/versions/published" target="_blank">
              <i class="fa fa-info-circle text-info"></i>
            </a>
            <!--<button type="button" id="btn-benefits-help" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Help">
              <i class="fa fa-2x fa-question-circle text-info"></i>
            </button>-->
          </div>
        </div>
        <div class="box-body box-benefits-body">
          <div class="form-group">
            <label for="service-benefit" class="col-sm-3 control-label benefits-label">Customer/Community</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="service-benefit" class="benefits" value="1" {{ $model->InitBenefits->Service == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="service-benefit" class="benefits" value="0" {{ $model->InitBenefits->Service == '0' ? 'checked' : '' }}> No
                </label>
              </div>
              <span class="error-service-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group service-benefit-details {{ $model->InitBenefits->Service == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="service-benefit-details" class="col-sm-3 control-label"><!--Details <span class="required text-danger">*</span>--></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="service-benefit-details" id="service-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative improves service delivery and effectiveness from a customer or community perspective" {{ $model->InitBenefits->Service == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->ServiceDetails }}</textarea>
              <span class="error-service-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="quality-benefit" class="col-sm-3 control-label benefits-label">Quality</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="quality-benefit" class="benefits" value="1" {{ $model->InitBenefits->Quality == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="quality-benefit" class="benefits" value="0" {{ $model->InitBenefits->Quality == '0' ? 'checked' : '' }}> No
                </label>
              </div>
              <span class="error-quality-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group quality-benefit-details {{ $model->InitBenefits->Quality == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="quality-benefit-details" class="col-sm-3 control-label"><!--Details <span class="required text-danger">*</span>--></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="quality-benefit-details" id="quality-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative improves quality e.g. defects, predictability or capacity" {{ $model->InitBenefits->Quality == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->QualityDetails }}</textarea>
              <span class="error-quality-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="people-benefit" class="col-sm-3 control-label benefits-label">People</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="people-benefit" class="benefits" value="1" {{ $model->InitBenefits->People == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="people-benefit" class="benefits" value="0" {{ $model->InitBenefits->People == '0' ? 'checked' : '' }}> No
                </label>
              </div>
              <span class="error-people-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group people-benefit-details {{ $model->InitBenefits->People == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="people-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="people-benefit-details" id="people-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative improves work culture/morale, staff development/skills and staff capacity" {{ $model->InitBenefits->People == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->PeopleDetails }}</textarea>
              <span class="error-people-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="environmental-benefit" class="col-sm-3 control-label benefits-label">Environmental</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="environmental-benefit" class="benefits" value="1" {{ $model->InitBenefits->Enviro == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="environmental-benefit" class="benefits" value="0" {{ $model->InitBenefits->Enviro == '0' ? 'checked' : '' }}> No
                  <span class="error-environmental-benefit text-danger"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group environmental-benefit-details {{ $model->InitBenefits->Enviro == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="environmental-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="environmental-benefit-details" id="environmental-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative improves and maintains natural and/or built assets" {{ $model->InitBenefits->Enviro == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->EnviroDetails }}</textarea>
              <span class="error-environmental-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="risk-benefit" class="col-sm-3 control-label benefits-label">Risk</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="risk-benefit" class="benefits" value="1" {{ $model->InitBenefits->Risk == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="risk-benefit" class="benefits" value="0" {{ $model->InitBenefits->Risk == '0' ? 'checked' : '' }}> No
                </label>
              </div>
              <span class="error-risk-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group risk-benefit-details {{ $model->InitBenefits->Risk == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="risk-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="risk-benefit-details" id="risk-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative can reduce or mitigate safety, reputational, environmental and asset risks" {{ $model->InitBenefits->Risk == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->RiskDetails }}</textarea>
              <span class="error-risk-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="economic-benefit" class="col-sm-3 control-label benefits-label">Economic</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="economic-benefit" class="benefits" value="1" {{ $model->InitBenefits->Eco == '1' || $isNewForm ? 'checked' : '' }}> Yes
                </label>
                <label>
                  <input type="radio" name="economic-benefit" class="benefits" value="0" {{ $model->InitBenefits->Eco == '0' ? 'checked' : '' }}> No
                </label>
              </div>
              <span class="error-economic-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group economic-benefit-details {{ $model->InitBenefits->Eco == '1' || $isNewForm ? '' : 'hide' }}">
            <label for="economic-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="economic-benefit-details" id="economic-benefit-details" class="form-control" rows="3"
                        placeholder="Describe how this Initiative helps in reducing or avoiding costs and revenue generation." {{ $model->InitBenefits->Eco == '1' || $isNewForm ? 'required' : '' }}>{{ $model->InitBenefits->EcoDetails }}</textarea>
              <span class="error-economic-benefit-details text-danger"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-solid box-additional-info">
        <div class="box-header with-border">
          <div class="box-title text-primary">Additional Info </div>
        </div>
        <div class="box-body box-additional-info-body">
          @if ($isEditForm)
          <div class="form-group">
            <label for="phase-id" class="col-sm-3 control-label">Phase <span class="required text-danger">*</span></label>
            <div class="col-sm-9">
              <select name="phase-id" id="phase-id" class="form-control">
                @foreach ($phases as $phase)
                <option value="{{ $phase->PhaseID }}" {{ $phase->PhaseID == $model->PhaseID ? 'selected' : '' }}>{{ $phase->Phase }}</option>
                @endforeach
              </select>
              <span class="error-phase-id text-danger"></span>
            </div>
          </div>
          @endif
          <div class="form-group references-group">
            <label for="references" class="col-sm-3 control-label">References </label>
            <div class="col-sm-9">
              <!-- Single button -->
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Add reference <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="javascript:void(0)" id="add-file-ref">File attachment</a></li>
                  <li><a href="javascript:void(0)" id="add-url-ref">Website URL</a></li>
                  <li><a href="javascript:void(0)" id="add-comment-ref">Text (Reference ID)</a></li>
                </ul>
              </div>
              @if ($model->InitReferences && $model->InitReferences->count() > 0)
              <p></p>
              <div class="box box-widget">
                <div class="box-body box-comments box-comments-no-image">
                  @foreach ($model->InitReferences as $ref)
                  <div class="box-comment had-old-ref">
                    <div class="comment-text">
                      <span class="username">
                        <a href="{{ url('remove-reference') }}" class="btn-remove-exist-ref" data-ref-id="{{ $ref->ID }}"><i class="fa fa-remove text-danger"></i></a>
                        @if ($ref->Type == App\Models\InitReferences::TYPE_TEXT)
                        {{ $ref->Content }}
                        @elseif ($ref->Type == App\Models\InitReferences::TYPE_URL)
                        <a target="_blank" href="{{ $ref->Content }}">{{ $ref->Content }}</a>
                        @else
                        <a target="_blank" href="{{ url('reference-file/' . $ref->Content) }}">{{ $ref->Content }}</a>
                        @endif
                        <span class="text-muted pull-right">
                          <span class="label label-default">{{ $ref->DateCreated }}</span>
                        </span>
                        <br>
                        <i class="text-muted">{{ $ref->Description }}</i>
                      </span>
                    </div>
                    <!-- /.comment-text -->
                  </div>
                  <!-- /.box-comment -->
                  @endforeach
                </div>
              </div>
              @endif
            </div>
          </div>
          @if ($isEditForm)
          <div class="form-group completion-date hide">
            <label for="completion-date" class="col-sm-3 control-label">Completion Date <span class="required text-danger">*</span></label>
            <div class="col-sm-9">
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="completion-date" id="completion-date" class="form-control pull-right" value="{{ $model->CompleteDate }}">
              </div>
              <span class="error-completion-date text-danger"></span>
            </div>
          </div>
          @endif
          <div class="form-group">
            <label for="comments" class="col-sm-3 control-label">Comments <span class="required text-danger hide">*</span></label>
            <div class="col-sm-9">
              <textarea name="comments" id="comments" class="form-control" rows="3"></textarea>
              <span class="error-comments text-danger"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if ($isEditForm)
    @include('initiatives._audit_trail')
    @endif
  </div>
  <div class="row">
    <div class="col-sm-offset-5 col-sm-2 text-center">
      <button type="submit" class="btn btn-block btn-darkblue btn-default btn-main"><i class="fa fa-save"></i> Submit <i class="fa fa-spin fa-spinner hide loading"></i></button>
    </div>
  </div>
</form>
@endsection

@section('js')
<script>
  var PHASE_ACTIVE = {{ App\Models\Phase::ACTIVE }};
  var PHASE_ONHOLD = {{ App\Models\Phase::ONHOLD }};
  var PHASE_CANCELLED = {{ App\Models\Phase::CANCELLED }};
  var PHASE_COMPLETED = {{ App\Models\Phase::COMPLETED }};
  var PHASE_NEW = {{ App\Models\Phase::PHASE_NEW }};
  var CROSS_ORGANISATIONAL_INITIATIVE = {{ App\Models\Classification::CROSS_ORGANISATIONAL_INITIATIVE }};
  var LOCAL_WORKPLACE_INITIATIVE = {{ App\Models\Classification::LOCAL_WORKPLACE_INITIATIVE }};
  var aOfficerIsManagerOrDirector = $('#actioning-officer').find(':selected').data('is-manager-or-director');
</script>
<script src="{{ asset('js/init-improvement/create-update-form.js') }}"></script>
@endsection
