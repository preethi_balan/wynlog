@extends('layouts.app')

@section('css')

@endsection

@section('content-header')
{{ $model->isImprovement() ? 'View Improvement ' : 'View Initiative ' }}
#{{ $model->id_str }}
<div class="pull-right" id="button-wr">
  <a class="btn btn-default btn-darkblue" target="_blank" href="?print=true"><i class="fa fa-print"></i> Print</a>
  @if (Auth::user()->can('edit-initiative', $model))
  <a class="btn btn-default btn-darkblue" href="{{ url('edit-initiative', [$model]) }}"><i class="fa fa-edit"></i> Edit</a>
  @endif
</div>
@endsection

@section('content')
@if (session()->has('error_message'))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session()->get('error_message') }}
    </div>
  </div>
</div>
@endif
<!-- form start -->
<div class="form-horizontal">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-solid box-initiative">
            <div class="box-header with-border">
              <div class="box-title text-primary">General Info</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body box-initiative-body">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="created" class="col-sm-3 control-label">Created</span></label>
                <div class="col-sm-9">
                  <input type="text" name="created" class="form-control select2" id="actioning-officer" value="{{ date('Y-m-d', strtotime($model->DateCreated)) . '  -  ' . $model->Name }}" disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-3 control-label">{{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }}Title</span></label>
                <div class="col-sm-9">
                  <input type="text" maxlength="150" name="title" class="form-control" value="{{ $model->Title }}" disabled>
                  <span class="error-title text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="description" class="col-sm-3 control-label">Description</span></label>
                <div class="col-sm-9">
                  <textarea name="description" id="description" class="form-control" rows="3" disabled>{{ $model->Description }}</textarea>
                  <span class="error-description text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="actioning-officer" class="col-sm-3 control-label">Actioning Officer</span></label>
                <div class="col-sm-9">
                  <input type="text" name="actioning-officer" class="form-control select2" id="actioning-officer" value="{{ $model->ActioningOfficerUser ? $model->ActioningOfficerUser->name : $model->ActioningOfficer }}" disabled>
                  <span class="error-actioning-officer text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="classification-id" class="col-sm-3 control-label">CI Classification</span></label>
                <div class="col-sm-9">
                  <input type="text" name="classification-id" class="form-control" id="classification-id" value="{{ $model->Classification->Classification }}" disabled>
                  <span class="error-classification-id text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="directorate" class="col-sm-3 control-label">Directorate</span></label>
                <div class="col-sm-9">
                  <input type="text" name="directorate" class="form-control" id="directorate" value="{{ $model->Directorate }}" disabled>
                  <span class="error-directorate text-danger"></span>
                </div>
              </div>
              <div class="form-group {{ $model->Department ? '' : 'hide' }}">
                <label for="department" class="col-sm-3 control-label">Department</span></label>
                <div class="col-sm-9">
                  <input type="text" name="department" class="form-control" id="department" value="{{ $model->Department }}" disabled>
                  <span class="error-department text-danger"></span>
                </div>
              </div>
              <div class="form-group {{ $model->BusinessUnit ? '' : 'hide' }}">
                <label for="business-unit" class="col-sm-3 control-label">Business Unit</span></label>
                <div class="col-sm-9">
                  <input type="text" name="business-unit" class="form-control" id="business-unit" value="{{ $model->BusinessUnit }}" disabled>
                  <span class="error-business-unit text-danger"></span>
                </div>
              </div>
              @if ($model->ClassificationID == App\Models\Classification::CROSS_ORGANISATIONAL_INITIATIVE)
              <div class="form-group cross-department">
                <label for="cross-department" class="col-sm-3 control-label">Collaborating Departments</span></label>
                <div class="col-sm-9">
                  <select name="cross-department[]" id="cross-department" class="form-control select2" multiple disabled>
                    @foreach ($model->InitOtherDept as $dept)
                    <option selected>{{ $dept->Department }}</option>
                    @endforeach
                  </select>
                  <span class="error-cross-department text-danger"></span>
                </div>
              </div>
              @endif
              <div class="form-group supporting-departments">
                <label for="supporting-departments" class="col-sm-3 control-label">Supporting Departments</span></label>
                <div class="col-sm-9">
                  <p class="help-block help-block-top"></p>
                  <select name="supporting-departments[]" id="supporting-departments" class="form-control select2" multiple disabled data-placeholder=" ">
                    @foreach ($model->InitSupportDept as $dept)
                    <option selected>{{ $dept->Department }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="stakeholders" class="col-sm-3 control-label">Stakeholders</span></label>
                <div class="col-sm-9">
                  <textarea name="stakeholders" id="stakeholders" class="form-control" rows="3" disabled>{{ $model->Stakeholders }}</textarea>
                  <span class="error-stakeholders text-danger"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="project-sponsor" class="col-sm-3 control-label">Project Sponsor</span></label>
                <div class="col-sm-9">
                  <input type="text" name="project-sponsor" class="form-control select2" id="project-sponsor" value="{{ $model->Sponsor }}" disabled>
                  <span class="error-project-sponsor text-danger"></span>
                </div>
              </div>
              {{--<div class="form-group">
                <label for="service-id" class="col-sm-3 control-label">Service ID</span></label>
                <div class="col-sm-9">
                  <input type="text" name="service-id" class="form-control select2" id="service-id" value="{{ $model->ServiceCatalogue ? $model->ServiceCatalogue->Service : '' }}" disabled>
                  <span class="error-service-id text-danger"></span>
                </div>
              </div>--}}
              <div class="form-group">
                <label for="start-date" class="col-sm-3 control-label">Start Date</label>
                <div class="col-sm-9">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="start-date" id="start-date" class="form-control pull-right" value="{{ $model->StartDate }}" disabled>
                    <span class="error-start-date text-danger"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="confidential" class="col-sm-3 control-label">Confidential</label>
                <div class="checkbox col-sm-9">
                  <label><input type="checkbox" name="confidential" id="confidential" value="1" {{ $model->Confidential == 1 ? 'checked' : '' }} disabled> Hide this {{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }} from general view</label>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-12">
          <div class="box box-solid box-reporting-tags">
            <div class="box-header with-border">
              <div class="box-title text-primary">Reporting Tags</div>
            </div>
            <div class="box-body box-reporting-tags-body">
              <div class="form-group">
                <div class="col-sm-12">
                  From the options below, please check all that are applicable, if any. Identifying the appropriate tags will assist for reporting. <br>
                  More than one may be selected. If further explanation is required, please use the <b>comments</b> field. <br><br>
                  This Initiative is/will be:<br>
                </div>
                @foreach (App\Models\InitReportingTags::columns() as $column)
                <div class="col-sm-offset-1 col-sm-11">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="{{ $column['column'] }}" value="1" {{ $model->InitReportingTags && $model->InitReportingTags->{$column['column']} == 1 ? 'checked' : '' }} disabled> {!! $column['description_html'] !!}
                    </label>
                  </div>
                </div>
                @endforeach
                <div class="col-sm-offset-1 col-sm-11">
                  <div class="checkbox">
                    <label>
                      <input id="none-reporting-tag" type="checkbox" name="" value="" {{ !$model->getReportingTags() ? 'checked' : '' }} disabled> None of the above
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-solid box-benefits">
        <div class="box-header with-border">
          <div class="box-title text-primary">Benefit Categories</div>
        </div>
        <div class="box-body box-benefits-body">
          <div class="form-group">
            <label for="service-benefit" class="col-sm-3 control-label">Customer/Community</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="service-benefit" class="benefits" value="1" {{ $model->InitBenefits->Service == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="Service benefits could relate to the delivery or the effectiveness of core services." data-placement="bottom">
                  <input type="radio" name="service-benefit" class="benefits" value="0" {{ $model->InitBenefits->Service == 0 ? 'checked' : '' }} disabled> No
                </label>
              </div>
              <span class="error-service-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group service-benefit-details {{ $model->InitBenefits->Service == 0 ? 'hide' : '' }}">
            <label for="service-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="service-benefit-details" id="service-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->ServiceDetails }}</textarea>
              <span class="error-service-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="quality-benefit" class="col-sm-3 control-label">Quality</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="quality-benefit" class="benefits" value="1" {{ $model->InitBenefits->Quality == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="Quality benefits could relate to defects, predictability or capacity." data-placement="auto">
                  <input type="radio" name="quality-benefit" class="benefits" value="0" {{ $model->InitBenefits->Quality == 0 ? 'checked' : '' }} disabled> No
                </label>
              </div>
              <span class="error-quality-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group quality-benefit-details {{ $model->InitBenefits->Quality == 0 ? 'hide' : '' }}">
            <label for="quality-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="quality-benefit-details" id="quality-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->QualityDetails }}</textarea>
              <span class="error-quality-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="people-benefit" class="col-sm-3 control-label">People</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="people-benefit" class="benefits" value="1" {{ $model->InitBenefits->People == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="People benefits could relate to culture/morale, effort or learning." data-placement="auto">
                  <input type="radio" name="people-benefit" class="benefits" value="0" {{ $model->InitBenefits->People == 0 ? 'checked' : '' }} disabled> No
                </label>
              </div>
              <span class="error-people-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group people-benefit-details {{ $model->InitBenefits->People == 0 ? 'hide' : '' }}">
            <label for="people-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="people-benefit-details" id="people-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->PeopleDetails }}</textarea>
              <span class="error-people-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="environmental-benefit" class="col-sm-3 control-label">Environmental</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="environmental-benefit" class="benefits" value="1" {{ $model->InitBenefits->Enviro == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="Environmental benefits relating to improving and maintaining natural and built assets." data-placement="auto">
                  <input type="radio" name="environmental-benefit" class="benefits" value="0" {{ $model->InitBenefits->Enviro == 0 ? 'checked' : '' }} disabled> No
                  <span class="error-environmental-benefit text-danger"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group environmental-benefit-details {{ $model->InitBenefits->Enviro == 0 ? 'hide' : '' }}">
            <label for="environmental-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="environmental-benefit-details" id="environmental-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->EnviroDetails }}</textarea>
              <span class="error-environmental-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="risk-benefit" class="col-sm-3 control-label">Risk</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="risk-benefit" class="benefits" value="1" {{ $model->InitBenefits->Risk == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="Risk benefits could relate to safety, reputation or regulatory compliance." data-placement="bottom">
                  <input type="radio" name="risk-benefit" class="benefits" value="0" {{ $model->InitBenefits->Risk == 0 ? 'checked' : '' }} disabled> No
                </label>
              </div>
              <span class="error-risk-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group risk-benefit-details {{ $model->InitBenefits->Risk == 0 ? 'hide' : '' }}">
            <label for="risk-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="risk-benefit-details" id="risk-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->RiskDetails }}</textarea>
              <span class="error-risk-benefit-details text-danger"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="economic-benefit" class="col-sm-3 control-label">Economic</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input type="radio" name="economic-benefit" class="benefits" value="1" {{ $model->InitBenefits->Eco == 1 ? 'checked' : '' }} disabled> Yes
                </label>
                <label data-toggle="tooltip" title="Economic benefits could relate to efficiency/cost reduction, opportunity, capability or revenue generation." data-placement="auto">
                  <input type="radio" name="economic-benefit" class="benefits" value="0" {{ $model->InitBenefits->Eco == 0 ? 'checked' : '' }} disabled> No
                </label>
              </div>
              <span class="error-economic-benefit text-danger"></span>
            </div>
          </div>
          <div class="form-group economic-benefit-details {{ $model->InitBenefits->Eco == 0 ? 'hide' : '' }}">
            <label for="economic-benefit-details" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <textarea maxlength="500" name="economic-benefit-details" id="economic-benefit-details" class="form-control" rows="3" disabled>{{ $model->InitBenefits->EcoDetails }}</textarea>
              <span class="error-economic-benefit-details text-danger"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-solid box-additional-info">
        <div class="box-header with-border">
          <div class="box-title text-primary">Additional Info</div>
        </div>
        <div class="box-body box-additional-info-body">
          <div class="form-group">
            <label for="phase-id" class="col-sm-3 control-label">Phase</span></label>
            <div class="col-sm-9">
              <input type="text" name="phase-id" id="phase-id" class="form-control" value="{{ $model->Phase->Phase }}" disabled>
              <span class="error-phase-id text-danger"></span>
            </div>
          </div>
          <div class="form-group references-group">
            <label for="references" class="col-sm-3 control-label">References</label>
            <div class="col-sm-9">
              @if ($model->InitReferences && $model->InitReferences->count() > 0)
              <p></p>
              <div class="box box-widget">
                <div class="box-body box-comments box-comments-no-image">
                  @foreach ($model->InitReferences as $ref)
                  <div class="box-comment">
                    <div class="comment-text">
                      <span class="username">
                        @if ($ref->Type == App\Models\InitReferences::TYPE_TEXT)
                        {{ $ref->Content }}
                        @elseif ($ref->Type == App\Models\InitReferences::TYPE_URL)
                        <a target="_blank" href="{{ $ref->Content }}">{{ $ref->Content }}</a>
                        @else
                        <a target="_blank" href="{{ url('reference-file/' . $ref->Content) }}">{{ $ref->Content }}</a>
                        @endif
                        <span class="text-muted pull-right">
                          <span class="label label-default">{{ $ref->DateCreated }}</span>
                        </span>
                        <br>
                        <i class="text-muted">{{ $ref->Description }}</i>
                      </span>
                    </div>
                    <!-- /.comment-text -->
                  </div>
                  <!-- /.box-comment -->
                  @endforeach
                </div>
              </div>
              @else
              <div class="box box-widget">
                <div class="box-body"><i>No references found</i>
                </div>
              </div>
              @endif
            </div>
          </div>
          @if ($model->CompleteDate)
          <div class="form-group completion-date">
            <label for="completion-date" class="col-sm-3 control-label">Completion Date</span></label>
            <div class="col-sm-9">
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="completion-date" id="completion-date" class="form-control pull-right" value="{{ $model->CompleteDate }}" disabled>
              </div>
              <span class="error-completion-date text-danger"></span>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
    @include('initiatives._audit_trail')
  </div>
</div>
@endsection

@section('js')
<script>
  $(document).ready(function() {
    $('#cross-department').chosen({});
    $('#supporting-departments').chosen({});

    var scrollBarOpts = {
      theme:"dark-3",
      scrollButtons: {
        enable: true,
        scrollAmount: 10
      },
    };
    $(".box-benefits-body").css('max-height', '600px');
    $(".box-benefits-body").mCustomScrollbar(scrollBarOpts);

    $(".box-additional-info-body").css('max-height', ($('.box-initiative-body')[0].clientHeight + $('.box-reporting-tags-body')[0].clientHeight - $(".box-benefits-body")[0].clientHeight) + 'px');
    $(".box-additional-info-body").mCustomScrollbar(scrollBarOpts);
  });
</script>
@endsection
