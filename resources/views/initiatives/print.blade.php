<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Wyn-Log Initiatives & Improvements Register</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  {{-- Custom CSS --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body id="print-view">
  <div class="print-header">
    <div class="container-fluid">
      <h1 class="pull-left"><img src="../images/wynlog-logo-33x50.png"> Wyn-Log <small>Initiatives & Improvements Register</small></h1>
      <span class="text-uppercase pull-right">
        {{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }} #{{ $model->id_str }}
      </span>
    </div>
  </div>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h5>{{ $model->isImprovement() ? 'Improvement ' : 'Initiative ' }}</h5>
          <h3>{{ $model->Title }}</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <table class="table table-bordered">
            <tr>
              <th>Created</th>
              <td>{{ date('Y-m-d', strtotime($model->DateCreated)) }} by {{ $model->Name }}</td>
            </tr>
            <tr>
              <th>Directorate</th>
              <td>{{ $model->Directorate }}</td>
            </tr>
            <tr>
              <th>Department</th>
              <td>{{ $model->Department }}</td>
            </tr>
            <tr>
              <th>Business Unit</th>
              <td>{{ $model->BusinessUnit }}</td>
            </tr>
          </table>
        </div>
        <div class="col-xs-6">
          <table class="table table-bordered">
            <tr>
              <th>Project Sponsor</th>
              <td>{{ $model->Sponsor }}</td>
            </tr>
            <tr>
              <th>Actioning Officer</th>
              <td>{{ $model->ActioningOfficerUser ? $model->ActioningOfficerUser->name : $model->ActioningOfficer }}</td>
            </tr>
            <tr>
              <th>Start Date</th>
              <td>{{ $model->StartDate }}</td>
            </tr>
            <tr>
              <th>Confidential?</th>
              <td>{{ $model->Confidential ? 'Yes' : 'No' }}</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered">
            <tr>
              <th>Description</th>
              <td>{{ $model->Description }}</td>
            </tr>
            <tr>
              <th>CI Classification</th>
              <td>{{ $model->Classification->Classification }}</td>
            </tr>
            @if ($model->ClassificationID == App\Models\Classification::CROSS_ORGANISATIONAL_INITIATIVE)
            <tr>
              <th>Collaborating Departments</th>
              <td>
                {{ $model->InitOtherDept->implode('Department', ', ') }}
              </td>
            </tr>
            @endif
            <tr>
              <th>Supporting Departments</th>
              <td>
                {{ $model->InitSupportDept->implode('Department', ', ') }}
              </td>
            <tr>
              <th>Stakeholders</th>
              <td>{{ $model->Stakeholders }}</td>
            </tr>
            {{--<tr>
              <th>Service ID</th>
              <td>
                {{ $model->ServiceCatalogue ? $model->ServiceCatalogue->Service : '' }}
              </td>
            </tr>--}}
            <tr>
              <th>Reporting Tags</th>
              <td>
                @foreach (App\Models\InitReportingTags::columns() as $column)
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="{{ $column['column'] }}" value="1" {{ $model->InitReportingTags && $model->InitReportingTags->{$column['column']} == 1 ? 'checked' : '' }} disabled> {!! $column['description_html'] !!}
                  </label>
                </div>
                @endforeach
                <div class="checkbox">
                  <label>
                    <input id="none-reporting-tag" type="checkbox" name="" value="" {{ !$model->getReportingTags() ? 'checked' : '' }} disabled> None of the above
                  </label>
                </div>
              </td>
            </tr>
            <tr>
              <th>Benefits</th>
              <?php $initBenefits = $model->InitBenefits; ?>
              <td>
                <strong>Customer/Community: </strong>{{ $initBenefits->Service ? $initBenefits->ServiceDetails : 'N/A' }}<br>
                <strong>Quality: </strong>{{ $initBenefits->Quality ? $initBenefits->QualityDetails : 'N/A' }}<br>
                <strong>People: </strong>{{ $initBenefits->People ? $initBenefits->PeopleDetails : 'N/A' }}<br>
                <strong>Environmental: </strong>{{ $initBenefits->Enviro ? $initBenefits->EnviroDetails : 'N/A' }}<br>
                <strong>Risk: </strong>{{ $initBenefits->Risk ? $initBenefits->RiskDetails : 'N/A' }}<br>
                <strong>Economic: </strong>{{ $initBenefits->Eco ? $initBenefits->EcoDetails : 'N/A' }}<br>
              </td>
            </tr>
            <tr>
              <th>Phase</th>
              <td>{{ $model->Phase->Phase }}</td>
            </tr>
            @if ($model->CompleteDate)
            <tr>
              <th>Completion Date</th>
              <td>{{ $model->CompleteDate }}</td>
            </tr>
            @endif
            <tr>
              <th>References</th>
              <td>
                @if ($model->InitReferences && $model->InitReferences->count() > 0)
                  @foreach ($model->InitReferences as $ref)
                  <p>&bullet;
                    <span class="text-muted">
                      <span class="label label-default">{{ $ref->DateCreated }}</span>
                    </span>
                    @if ($ref->Type == App\Models\InitReferences::TYPE_TEXT)
                    {{ $ref->Content }}
                    @elseif ($ref->Type == App\Models\InitReferences::TYPE_URL)
                    <a target="_blank" href="{{ $ref->Content }}">{{ $ref->Content }}</a>
                    @else
                    <a target="_blank" href="{{ url('reference-file/' . $ref->Content) }}">{{ $ref->Content }}</a>
                    @endif
                    <br>
                    <i class="text-muted">{{ $ref->Description }}</i>
                  </p>
                  @endforeach
                @else
                  <i>N/A</i>
                @endif
              </td>
            </tr>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered">
            <tr>
              <th>Comments</th>
              <td>
                @if ($model->InitComments && $model->InitComments->count() > 0)
                  @foreach ($model->InitComments as $initComment)
                  <p>&bullet;
                    {{ $initComment->Username }} on
                    <span class="text-muted">
                      <span class="label label-default">Phase {{ $initComment->Phase->Phase }}</span>
                      <span class="label label-default">{{ $initComment->DateCreated }}</span>
                    </span>:
                    {{ $initComment->Comments }}
                  </p>
                  @endforeach
                @endif
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  <script>
    window.print();
  </script>
</body>
</html>
