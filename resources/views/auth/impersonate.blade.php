@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Select user to impersonate</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/impersonate') }}">
                        {{ csrf_field() }}
                        @if ($errors->has('error'))
                        <div class="alert alert-warning">
                            {{$errors->first('error')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        @endif

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <!--<label for="username" class="col-md-2 control-label">Username</label>-->

                            <div class="col-md-12">
                                <select name="username" class="form-control select-user" required data-placeholder="Search for name, username or position title...">
                                    <option></option>
                                    @foreach ($users as $user)
                                    <option value="{{ $user->username }}">{{ $user->name }} ({{ $user->username }}) - {{ $user->positionTitle }}{{ $user->department ? ' (' . $user->department . ')' : '' }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group pull-right">
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Impersonate
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('.select-user').chosen({
        width: '100%'
    });
});
</script>
@endsection
