<?php

namespace App\Services\Validation;

use Illuminate\Validation\Validator as IlluminateValidator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ValidatorExtended extends IlluminateValidator {

    public function __construct($translator, $data, $rules, $messages = array(), $customAttributes = array()) {
        parent::__construct($translator, $data, $rules, $messages, $customAttributes);

        // Implicit 'max' validation rule to make it work with invalid uploaded file
        // http://php.net/manual/en/features.file-upload.errors.php
        // https://github.com/laravel/framework/issues/8203
        $this->implicitRules[] = 'MaxFileSize';

        $this->setCustomMessages([
            'max_file_size' => 'Maximum file size is ' . UploadedFile::getMaxFilesize() / 1024 / 1024 . 'MB.'
        ]);
    }

    /**
     * Return validation message if the uploaded file exceeds MAX_FILE_SIZE
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    protected function validateMaxFileSize($attribute, $value) {
        if ($value instanceof UploadedFile && $value->getError() == UPLOAD_ERR_INI_SIZE) {
            return false;
        }

        return true;
    }
}