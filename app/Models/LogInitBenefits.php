<?php

namespace App\Models;

class LogInitBenefits extends BaseModel
{
    protected $table = 'Log_Init_Benefits';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'LogActionedDate';
    const UPDATED_AT = 'LogActionedDate';
}
