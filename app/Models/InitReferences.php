<?php

namespace App\Models;

class InitReferences extends BaseModel
{
    protected $table = 'Init_References';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const TYPE_TEXT = 'Text';
    const TYPE_URL = 'Url';
    const TYPE_FILE = 'File';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InitID', 'Type', 'Content', 'Description'];
}
