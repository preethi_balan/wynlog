<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class AD extends Authenticatable
{
    protected $connection = 'sqlsrv2';
    protected $table = 'AD';
    protected $primaryKey = 'WAMI';
    public $timestamps = false;

    const ADMIN_DEPARTMENT = 'Excellence@Wyndham';
    const DEV_UNIT = 'System Development and An';

    const CEO_LEVEL = 7;
    const DIRECTOR_LEVEL = 6;
    const MANAGER_LEVEL = 5;

    private $pa2CeoPositions = [
        200003,
    ];

    public function isAdmin()
    {
        return strcasecmp(trim($this->department), self::ADMIN_DEPARTMENT) === 0
            || strcasecmp(trim($this->group), self::DEV_UNIT) === 0;
    }

    public function isManagerOrDirector()
    {
        return $this->isManager() || $this->isDirector();
    }

    public function isCEO()
    {
        return $this->authorityLevel == self::CEO_LEVEL || in_array($this->position, $this->pa2CeoPositions);
    }

    public function isManager()
    {
        return $this->authorityLevel == self::MANAGER_LEVEL;
    }

    public function isDirector()
    {
        return $this->authorityLevel == self::DIRECTOR_LEVEL;
    }

    public function hasNoDepartment() //CEO, Directors, PA to Director/CEO
    {
        return !$this->department;
    }

    /**
     * Get positions number of user
     * @return array
     */
    public function getPositions()
    {
        $positionNos = [];
        if ($this->coordinatorPosition) {
            $positionNos[] = $this->coordinatorPosition;
        }
        if ($this->managerPosition) {
            $positionNos[] = $this->managerPosition;

            /*
            // #### Fixed - Updated code to use Aurion_Orgstructure for director position - 12/10/16 ####
            $directorPos = DB::connection('sqlsrv2')
                ->table('Aurion_OrgStructure')
                ->select('ReportsToPosition')
                ->where('Position', '=', $this->managerPosition)
                ->get();

            foreach ($directorPos as $positions) {
                if (!empty($positions->ReportsToPosition)) {
                    if (!in_array($positions->ReportsToPosition, $positionNos)) {
                        $positionNos[] = $positions->ReportsToPosition;
                    }
                }
            }
            */
        }
        if ($this->directorPosition) {
            $positionNos[] = $this->directorPosition;
        }

        return $positionNos;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function setImpersonating($userId)
    {
        Session::put('impersonate', $userId);
    }

    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return Session::has('impersonate');
    }
}
