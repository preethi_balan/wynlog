<?php

namespace App\Models;

class LogInitiatives extends BaseModel
{
    protected $table = 'Log_Initiatives';
    protected $primaryKey = "ID";

    const CREATED_AT = 'LogActionedDate';
    const UPDATED_AT = 'LogActionedDate';

    protected $with = [
        'Classification',
        'Phase',
        'LogActionedByUser',
    ];

    public function Classification()
    {
        return $this->hasOne('App\Models\Classification', 'ClassID', 'ClassificationID');
    }

    public function Phase()
    {
        return $this->hasOne('App\Models\Phase', 'PhaseID', 'PhaseID');
    }

    public function LogActionedByUser()
    {
        return $this->hasOne('App\Models\AD', 'username', 'LogActionedBy');
    }

    public function compareWithOriginal($originalInit = null)
    {
        if (!$originalInit) {
            $originalInit = Initiatives::find($this->InitID);
        }

        if ($originalInit) {

        }
    }
}
