<?php

namespace App\Models;

class LogViews extends BaseModel
{
    protected $table = 'Log_Views';
    protected $primaryKey = "InitID";
    
    const CREATED_AT = 'ViewedDate';
    const UPDATED_AT = 'ViewedDate';
}
