<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ADCopy extends Authenticatable
{
    protected $table = 'AD_Copy';
    protected $primaryKey = "WAMI";
    public $timestamps = false;

    const ADMIN_DEPARTMENT = 'Excellence@Wyndham';

    public function isAdmin()
    {
        return strcasecmp($this->department, self::ADMIN_DEPARTMENT) === 0;
    }

    /**
     * Get positions number of user
     * @return array
     */
    public function getPositions()
    {
        $positionNos = [];
        if ($this->coordinatorPosition) {
            $positionNos[] = $this->coordinatorPosition;
        }
        if ($this->managerPosition) {
            $positionNos[] = $this->managerPosition;
        }
        if ($this->directorPosition) {
            $positionNos[] = $this->directorPosition;
        }
        return $positionNos;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

}
