<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitBenefits extends Model
{
    protected $table = 'Init_Benefits';
    protected $primaryKey = "InitID";
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function LogInitBenefits()
    {
        return $this->hasMany('App\Models\LogInitBenefits', 'InitID');
    }

    public function getServiceBenefit()
    {
        return $this->ServiceBenefit;
    }
}
