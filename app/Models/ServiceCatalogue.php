<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCatalogue extends Model
{
    protected $table = 'ServiceCatalogue';
    protected $primaryKey = "ID";
    public $timestamps = false;
}
