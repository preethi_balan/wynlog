<?php

namespace App\Models;

class LogInitReportingTags extends BaseModel
{
    protected $table = 'Log_Init_ReportingTags';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'LogActionedDate';
    const UPDATED_AT = 'LogActionedDate';
}
