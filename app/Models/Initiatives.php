<?php

namespace App\Models;
use Auth;

class Initiatives extends BaseModel
{
    protected $table = 'Initiatives';
    protected $primaryKey = "ID";

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'LastUpdated';

    protected $appends = ['id_str'];

    // Because modify ID made model eager loading not work
    // So create another attribute instead
    public function getIdStrAttribute()
    {
        return str_pad($this->getKey(), 5, '0', STR_PAD_LEFT);
    }

    public function getRouteKey()
    {
        return $this->id_str;
    }

    public function isImprovement()
    {
        return $this->PhaseID == Phase::COMPLETED;
    }

    public function scopeWithRelatedData($query)
    {
        return $query->with([
            'Classification',
            'Phase',
            'InitBenefits',
            'InitReportingTags',
            'InitOtherDept',
            'InitSupportDept',
            'InitUserAccess',
            'InitComments',
            'InitReferences',
            'ActioningOfficerUser',
        ]);
    }

    public function scopeSelectImprovements($query)
    {
        return $query->where('PhaseID', Phase::COMPLETED);
    }

    public function scopeSelectInitiatives($query)
    {
        return $query->where('PhaseID', '!=', Phase::COMPLETED);
    }

    public function scopeListInitsByUserType($query)
    {
        $user = Auth::user();

        $query->selectInitiatives();

        if ($user->isAdmin() || $user->isCEO()) {
            return $query;
        }

        if ($user->hasNoDepartment()) {
            $directorateDepts = AD::select('department')
                ->where('directorate', '=', $user->directorate)
                ->whereNotNull('department')
                ->distinct()
                ->pluck('department');

            $query->where(function ($initQuery) use ($user, $directorateDepts) {
                $initQuery->where('Directorate', '=', $user->directorate)
                    ->orWhere('Username', '=', $user->username)
                    ->orWhere('ActioningOfficer', '=', $user->username)
                    // Relevant Cross-Organisational Initiative
                    ->orWhereHas('InitOtherDept', function($otherDeptQuery) use ($directorateDepts) {
                        $otherDeptQuery->whereIn('Department', $directorateDepts);
                    })
                    ->orWhereHas('InitSupportDept', function($supportDeptQuery) use ($directorateDepts) {
                        $supportDeptQuery->whereIn('Department', $directorateDepts);
                    });
            });

            return $query;
        }

        $query->where(function ($initQuery) use ($user) {
            $initQuery->where('Department', '=', $user->department)
                ->orWhere('Username', '=', $user->username)
                ->orWhere('ActioningOfficer', '=', $user->username)
                // Relevant Cross-Organisational Initiative
                ->orWhereHas('InitOtherDept', function($otherDeptQuery) use ($user) {
                    $otherDeptQuery->where('Department', '=', $user->department);
                })
                ->orWhereHas('InitSupportDept', function($supportDeptQuery) use ($user) {
                    $supportDeptQuery->where('Department', '=', $user->department);
                });
        });

        return $query;
    }

    public function scopeDashboardDataByUserType($query)
    {
        $user = Auth::user();

        if ($user->isCEO()) {
            return $query;
        }

        if ($user->hasNoDepartment() || $user->isDirector()) {
            $query->where('Directorate', '=', $user->directorate);
        } else {
            $query->where('Department', '=', $user->department);
        }

        $query->where(function ($initQuery) use ($user) {
            $initQuery->where('Confidential', 0);

            $initQuery->orWhere(function ($conQuery) use ($user) {
                $conQuery->where('Confidential', 1)
                    ->whereHas('InitUserAccess', function ($accessQuery) use ($user) {
                        $accessQuery->where('PositionNo', $user->position);
                    });
            });
        });

        return $query;
    }

    public function Classification()
    {
        return $this->hasOne('App\Models\Classification', 'ClassID', 'ClassificationID');
    }

    public function Phase()
    {
        return $this->hasOne('App\Models\Phase', 'PhaseID', 'PhaseID');
    }

    /*public function ServiceCatalogue()
    {
        return $this->hasOne('App\Models\ServiceCatalogue', 'ID', 'ServiceID');
    }*/

    public function InitBenefits()
    {
        return $this->hasOne('App\Models\InitBenefits', 'InitID');
    }

    public function InitReportingTags()
    {
        return $this->hasOne('App\Models\InitReportingTags', 'InitID');
    }

    public function InitOtherDept()
    {
        return $this->hasMany('App\Models\InitOtherDept', 'InitID');
    }

    public function InitSupportDept()
    {
        return $this->hasMany('App\Models\InitSupportDept', 'InitID');
    }

    public function InitUserAccess()
    {
        return $this->hasMany('App\Models\InitUserAccess', 'InitID');
    }

    public function InitComments()
    {
        return $this->hasMany('App\Models\InitComments', 'InitID');
    }

    public function LogInitiatives()
    {
        return $this->hasMany('App\Models\LogInitiatives', 'InitID')
            ->orderBy('LogActionedDate', 'DESC');
    }

    public function LogViews()
    {
        return $this->hasMany('App\Models\LogViews', 'InitID');
    }

    public function InitReferences()
    {
        return $this->hasMany('App\Models\InitReferences', 'InitID');
    }

    public function ActioningOfficerUser()
    {
        return $this->hasOne('App\Models\AD', 'username', 'ActioningOfficer');
    }

    public function getBenefitsDetails()
    {
        $benefits = $this->InitBenefits;
        $details = '';

        if ($benefits) {
            $details = ($benefits->Service ? "[Customer/Community: $benefits->ServiceDetails] " : ' ')
                . ($benefits->Risk ? "[Risk: $benefits->RiskDetails] " : ' ')
                . ($benefits->Quality ? "[Quality: $benefits->QualityDetails] " : ' ')
                . ($benefits->People ? "[People: $benefits->PeopleDetails] " : ' ')
                . ($benefits->Enviro ? "[Environment: $benefits->EnviroDetails] " : ' ')
                . ($benefits->Eco ? "[Economic: $benefits->EcoDetails] " : ' ');
        }

        return $details;
    }

    public function getServiceBenefit()
    {
        return $this->InitBenefits->ServiceDetails;
    }

    public function getQualityBenefit()
    {
        return $this->InitBenefits->QualityDetails;
    }

    public function getPeopleBenefit()
    {
        return $this->InitBenefits->PeopleDetails;
    }

    public function getEnviroBenefit()
    {
        return $this->InitBenefits->EnviroDetails;
    }

    public function getRiskBenefit()
    {
        return $this->InitBenefits->RiskDetails;
    }

    public function getEcoBenefit()
    {
        return $this->InitBenefits->EcoDetails;
    }


    public function getReportingTags()
    {
        $reportingTags = $this->InitReportingTags;

        $return = '';
        if ($reportingTags) {
            foreach (InitReportingTags::columns() as $columnDef) {
                if ($reportingTags->{$columnDef['column']} == 1) {
                    $return .= '[' . $columnDef['title'] . '] ';
                }
            }
        }

        return $return;
    }

    public function getComments()
    {
        $comments = $this->InitComments;
        if (!$comments || $comments->isEmpty()) {
            return '';
        }

        return '[' . $comments->implode('Comments', '] [') . ']';
    }

    public function getOtherDepts()
    {
        $otherDepts = $this->InitOtherDept;
        if (!$otherDepts || $otherDepts->isEmpty()) {
            return '';
        }

        return '[' . $otherDepts->implode('Department', '] [') . ']';
    }

    public function getSupportDepts()
    {
        $supportDepts = $this->InitSupportDept;
        if (!$supportDepts || $supportDepts->isEmpty()) {
            return '';
        }

        return '[' . $supportDepts->implode('Department', '] [') . ']';
    }

    public function getReferences()
    {
        $refs = $this->InitReferences;
        if (!$refs || $refs->isEmpty()) {
            return '';
        }

        return '[' . $refs->implode('Content', '] [') . ']';
    }
}
