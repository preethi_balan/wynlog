<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitReportingTags extends Model
{
    protected $table = 'Init_ReportingTags';
    protected $primaryKey = "InitID";
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function LogInitReportingTags()
    {
        return $this->hasMany('App\Models\LogInitReportingTags', 'InitID');
    }

    /**
     * Column definitions, use for dynamic generate form
     */
    public static function columns()
    {
        return [
            [
                'column' => 'PMF',
                'title' => 'Project Management Framework (PMF)',
                'description' => 'Managed through the Project Management Framework (PMF)',
                'description_html' => 'Managed through the <strong>Project Management Framework (PMF)</strong>',
            ],
            [
                'column' => 'Riskware',
                'title' => 'Riskware',
                'description' => 'Managed in Riskware',
                'description_html' => 'Managed in <strong>Riskware</strong>',
            ],
            [
                'column' => 'CityPlan',
                'title' => 'City Plan',
                'description' => 'Directly related to a City Plan initiative/theme',
                'description_html' => 'Directly related to a <strong>City Plan</strong> initiative/theme',
            ],
            [
                'column' => 'DeptPlan',
                'title' => 'Department Plan',
                'description' => 'Documented in an existing Department Plan',
                'description_html' => 'Documented in an existing <strong>Department Plan</strong>',
            ],
        ];
    }
}
