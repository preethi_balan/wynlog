<?php

namespace App\Models;

class LogInitSupportDept extends BaseModel
{
    protected $table = 'Log_Init_SupportDept';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'LogActionedDate';
    const UPDATED_AT = 'LogActionedDate';
}
