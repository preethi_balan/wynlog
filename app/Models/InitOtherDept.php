<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitOtherDept extends Model
{
    protected $table = 'Init_OtherDept';
    protected $primaryKey = "ID";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InitID', 'Department'];

    public function LogInitOtherDept()
    {
        return $this->hasMany('App\Models\LogInitOtherDept', 'RefID');
    }
}
