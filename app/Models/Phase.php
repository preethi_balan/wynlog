<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table = 'Phase';
    protected $primaryKey = "PhaseID";
    public $timestamps = false;

    const ACTIVE = 1;
    const ONHOLD = 2;
    const CANCELLED = 3;
    const COMPLETED = 4;
    const PHASE_NEW = 5;
}
