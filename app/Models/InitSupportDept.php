<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitSupportDept extends Model
{
    protected $table = 'Init_SupportDept';
    protected $primaryKey = "ID";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InitID', 'Department'];

    public function LogInitSupportDept()
    {
        return $this->hasMany('App\Models\LogInitSupportDept', 'RefID');
    }
}
