<?php

namespace App\Models;

class InitComments extends BaseModel
{
    protected $table = 'Init_Comments';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    protected $with = ['Phase'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InitID', 'Comments', 'Username', 'PhaseID'];

    public function Phase()
    {
        return $this->hasOne('App\Models\Phase', 'PhaseID', 'PhaseID');
    }
}
