<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    protected $table = 'Classification';
    protected $primaryKey = "ClassID";
    public $timestamps = false;

    const CROSS_ORGANISATIONAL_INITIATIVE = 1;
    const DEPARTMENTAL_INITIATIVE = 2;
    const LOCAL_WORKPLACE_INITIATIVE = 3;
}
