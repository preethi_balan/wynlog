<?php

namespace App\Models;

class LogInitOtherDept extends BaseModel
{
    protected $table = 'Log_Init_OtherDept';
    protected $primaryKey = "ID";
    
    const CREATED_AT = 'LogActionedDate';
    const UPDATED_AT = 'LogActionedDate';
}
