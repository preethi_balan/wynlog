<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitUserAccess extends Model
{
    protected $table = 'Init_UserAccess';
    protected $primaryKey = "ID";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InitID', 'PositionNo'];
}
