<?php

namespace App\Http\Controllers\Auth;

use App\Models\ADCopy;
use Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        $users = ADCopy::all();
        return view('auth.login')->with([
            'users' => $users
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('login');
    }

    public function authenticate()
    {
        $username = Request::input('username');
        $auth = ADCopy::where('username', '=', $username)->first();
        if ($auth) {
            Auth::login($auth);
            return redirect()->intended($this->redirectTo);
        } else {
            return redirect()->to('login')
                ->withErrors(['error' => "User doesn't exist."])
                ->withInput();
        }
    }

}
