<?php

namespace App\Http\Controllers;

use App\Models\AD;
use Auth;
use Request;

class UserController extends Controller
{
    public function showImpersonateForm()
    {
        $currentUser = Auth::user();

        $users = AD::where($currentUser->getKeyName(), '!=', $currentUser->getKey())
            ->get();
        return view('auth.impersonate')->with([
            'users' => $users,
        ]);
    }

    public function impersonate()
    {
        $username = Request::input('username');
        $user = AD::where('username', '=', $username)->first();
        if ($user) {
            Auth::user()->setImpersonating($user->getKey());
            return redirect()->intended('/');
        }
        
        return redirect()->back()
            ->withErrors(['error' => "User doesn't exist."])
            ->withInput();
    }

    public function stopImpersonate()
    {
        Auth::user()->stopImpersonating();

        return redirect()->intended('/');
    }
}
