<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Initiatives;
use App\Models\InitBenefits;
use App\Models\Phase;
use App\Models\AD;
use App\Models\Classification;
use Auth;
use DB;

class DashboardController extends Controller
{
    const INIT_TYPE = 'Initiatives';
    const IMP_TYPE = 'Improvements';

    static $ACTIVE_PHASES;
    private $initQuery;
    private $impQuery;
    private $addedQuery;
    private $appointedQuery;
    private $userInitQuery;

    public function __construct() {
        static::$ACTIVE_PHASES = [Phase::ACTIVE, Phase::ONHOLD, Phase::PHASE_NEW];

        parent::__construct();
    }

    public function index()
    {
        $recentInits = $this->getInitQuery()
            ->orderBy('DateCreated', 'desc')
            ->take(5)
            ->get();

        $recentImps = $this->getImpQuery()
            ->orderBy('CompleteDate', 'desc')
            ->take(5)
            ->get();

        $recentUserInit = $this->getUserInitQuery()
            ->orderBy('LastUpdated', 'desc')
            ->take(5)
            ->get();

        $user = Auth::user();
        $dashboardLabels = [];
        if ($user->isCEO()) {
            $dashboardLabels = [
                'overview' => 'Wyndham City Council',
                'past_12_months_by' => 'Directorate',
                'past_12_months_message' => 'organisation',
                'benefits_of' => 'organisation',
            ];
        } elseif ($user->hasNoDepartment()) {
            $dashboardLabels = [
                'overview' => $user->directorate,
                'past_12_months_by' => 'Department',
                'past_12_months_message' => 'directorate',
                'benefits_of' => 'directorate',
            ];
        } else {
            $dashboardLabels = [
                'overview' => $user->department,
                'past_12_months_by' =>  'Business Unit',
                'past_12_months_message' => 'department',
                'benefits_of' => 'department',
            ];
        }

        return view('dashboard.index')->with([
            'recentInits' => $recentInits,
            'recentImps' => $recentImps,
            'recentUserInit' => $recentUserInit,
            'dashboardLabels' => $dashboardLabels,
        ]);
    }

    private function getInitQuery($onlyActivePhases = true)
    {
        if (Auth::check()) {
            $this->initQuery = Initiatives::selectInitiatives()
                ->dashboardDataByUserType();

            if ($onlyActivePhases) {
                $this->initQuery
                    ->whereIn('Initiatives.PhaseID', static::$ACTIVE_PHASES);
            }

            return $this->initQuery;
        }
    }

    private function getImpQuery()
    {
        if (Auth::check()) {
            if (!$this->impQuery) {
                $this->impQuery = Initiatives::selectImprovements()
                    ->dashboardDataByUserType();
            }

            return $this->impQuery;
        }
    }

    private function getUserInitQuery()
    {
        if (Auth::check()) {
            if (!$this->userInitQuery) {
                $this->userInitQuery = Initiatives::query();
                $this->userInitQuery->where(function ($initQuery) {
                    $initQuery->where('Initiatives.ActioningOfficer', '=', Auth::user()->username)
                        ->orWhere('Initiatives.Username', '=', Auth::user()->username);
                });
            }

            return $this->userInitQuery;
        }
    }

    public function initCounts()
    {
        $months = $this->get12Months();
        $yearMonthQuery = "concat(year(DateCreated), '-', month(DateCreated))";

        // Active phases
        $counts = $this->getInitQuery()
            ->whereIn(DB::raw($yearMonthQuery), array_keys($months))
            ->select('PhaseID', DB::raw('count(*) as Total'))
            ->groupBy('PhaseID')
            ->pluck('Total', 'PhaseID');
        $phases = Phase::all()
            ->pluck('Phase', 'PhaseID');

        // Cancelled phase
        $initCanceledCounts = $this->getInitQuery(false)
            ->where('Initiatives.PhaseID', Phase::CANCELLED)
            ->whereIn(DB::raw($yearMonthQuery), array_keys($months))
            ->count();

        // Completed phase == Improvement
        $impCounts = $this->getImpQuery()
            ->whereIn(DB::raw($yearMonthQuery), array_keys($months))
            ->count();

        $chartLabels = [];
        $chartData = [];
        $totalActives = 0;
        foreach ($phases as $phaseID => $phaseName) {
            $chartLabels[] = $phaseName;
            if ($phaseID == Phase::CANCELLED) {
                $value = $initCanceledCounts;
            } elseif ($phaseID == Phase::COMPLETED) {
                $value = $impCounts;
            } else {
                $value = isset($counts[$phaseID]) ? (integer) $counts[$phaseID] : 0;
                $totalActives += $value;
            }

            $chartData[] = $value;
        }

        return [
            'labels' => $chartLabels,
            'values' => $chartData,
            'active' => $totalActives,
            'cancel' => $initCanceledCounts,
            'improvements' => $impCounts,
            'total' => $totalActives + $impCounts + $initCanceledCounts,
        ];
    }

    private function get12Months()
    {
        $yearMonthFormat = 'Y-n'; // Month without leading zero
        $yearMonthLabelFormat = 'M-y';
        $months = [];
        for ($i = 12; $i >= 0; --$i) {
            $time = strtotime("-$i month");
            $key = date($yearMonthFormat, $time);
            $months[$key] = date($yearMonthLabelFormat, $time);
        }

        return $months;
    }

    private function countsBy12Months($type)
    {
        $months = $this->get12Months();

        $query = $type == self::INIT_TYPE ? $this->getInitQuery() : $this->getImpQuery();
        $dateField = $type == self::INIT_TYPE ? 'DateCreated' : 'CompleteDate';
        $yearMonthQuery = "concat(year($dateField), '-', month($dateField))";
        $counts = $query->select(
                DB::raw('count(*) as Total'),
                DB::raw("$yearMonthQuery as YearMonth")
            )
            ->whereIn(DB::raw($yearMonthQuery), array_keys($months))
            ->groupBy(DB::raw($yearMonthQuery))
            ->pluck('Total', 'YearMonth');

        $chartData = [];
        $total = 0;
        foreach ($months as $key => $label) {
            $value = isset($counts[$key]) ? (integer) $counts[$key] : 0;
            $chartData[] = $value;
            $total += $value;
        }
        return [
            'labels' => array_values($months),
            'values' => $chartData,
            'total' => $total
        ];
    }

    public function initCountsBy12Months() {
        return $this->countsBy12Months(self::INIT_TYPE);
    }

    public function impCountsBy12Months() {
        return $this->countsBy12Months(self::IMP_TYPE);
    }

    private function countsPerUnitOfDept($type)
    {
        $businessUnits = AD::select('group')
            ->where('department', '=', Auth::user()->department)
            ->whereNotNull('group')
            ->distinct()
            ->pluck('group');

        $query = $type == self::INIT_TYPE ? $this->getInitQuery() : $this->getImpQuery();
        $dateField = $type == self::INIT_TYPE ? 'DateCreated' : 'CompleteDate';

        if ($businessUnits->count()) {
            $counts = $query
                ->select(
                    DB::raw('count(*) as Total'),
                    'BusinessUnit as BusinessUnitAlias',
                    'ClassificationID'
                )
                ->where($dateField, '>=', DB::raw('DATEADD(MONTH, -12, GETDATE())'))
                ->groupBy('BusinessUnit', 'ClassificationID')
                ->get();
        } else {
            // Depart has no business units
            // show the department as a business unit and display one bar on the bar chart
            $businessUnits[0] = Auth::user()->department;
            $counts = $query
                ->select(
                    DB::raw('count(*) as Total'),
                    'Department as BusinessUnitAlias',
                    'ClassificationID'
                )
                ->where($dateField, '>=', DB::raw('DATEADD(MONTH, -12, GETDATE())'))
                ->groupBy('Department', 'ClassificationID')
                ->get();
        }

        $classifications = Classification::pluck('Classification', 'ClassID');

        /*
        labels = [x1, x2, ...]; => business units
        datasets = [
            {y1, [x1_value, x2_value, ...]}
            {y2, [x1_value, x2_value, ...]}
            {y3, [x1_value, x2_value, ...]}
        ]
        => y1, y2, y3 => Classifications
        */
        $chartDatasets = [];
        $total = 0;
        foreach ($classifications as $classId => $className) {
            if ($type == self::IMP_TYPE) {
                $className = str_ireplace('Initiative', 'Improvement', $className);
            }

            $dataset = ['label' => $className];
            $data = array_fill(0, count($businessUnits), 0);
            foreach ($counts as $count) {
                if ($count->ClassificationID == $classId) {
                    foreach ($businessUnits as $index => $unit) {
                        $value = $count->BusinessUnitAlias == $unit ? (integer) $count->Total : 0;
                        $data[$index] += $value;
                        $total += $value;
                    }
                }
            }
            $dataset['data'] = $data;
            $chartDatasets[] = $dataset;
        }
        return [
            'labels' => $businessUnits,
            'datasets' => $chartDatasets,
            'total' => $total
        ];
    }

    private function countsPerDirectorate($type)
    {
        $directorates = AD::select('directorate')
            ->whereNotNull('directorate')
            ->distinct()
            ->pluck('directorate');

        $query = $type == self::INIT_TYPE ? $this->getInitQuery() : $this->getImpQuery();
        $dateField = $type == self::INIT_TYPE ? 'DateCreated' : 'CompleteDate';
        $counts = $query
            ->select(
                DB::raw('count(*) as Total'),
                'Directorate',
                'ClassificationID'
            )
            ->where($dateField, '>=', DB::raw('DATEADD(MONTH, -12, GETDATE())'))
            ->groupBy('Directorate', 'ClassificationID')
            ->get();
        $classifications = Classification::pluck('Classification', 'ClassID');

        /*
        labels = [x1, x2, ...]; => directorates
        datasets = [
            {y1, [x1_value, x2_value, ...]}
            {y2, [x1_value, x2_value, ...]}
            {y3, [x1_value, x2_value, ...]}
        ]
        => y1, y2, y3 => Classifications
        */
        $chartDatasets = [];
        $total = 0;
        foreach ($classifications as $classId => $className) {
            if ($type == self::IMP_TYPE) {
                $className = str_ireplace('Initiative', 'Improvement', $className);
            }

            $dataset = ['label' => $className];
            $data = array_fill(0, count($directorates), 0);
            foreach ($counts as $count) {
                if ($count->ClassificationID == $classId) {
                    foreach ($directorates as $index => $directorate) {
                        $value = $count->Directorate == $directorate ? (integer) $count->Total : 0;
                        $data[$index] += $value;
                        $total += $value;
                    }
                }
            }
            $dataset['data'] = $data;
            $chartDatasets[] = $dataset;
        }
        return [
            'labels' => $directorates,
            'datasets' => $chartDatasets,
            'total' => $total
        ];
    }

    private function countsPerDepartmentOfDirectorate($type)
    {
        $departments = AD::select('department')
            ->where('directorate', '=', Auth::user()->directorate)
            ->whereNotNull('department')
            ->distinct()
            ->pluck('department');

        $query = $type == self::INIT_TYPE ? $this->getInitQuery() : $this->getImpQuery();
        $dateField = $type == self::INIT_TYPE ? 'DateCreated' : 'CompleteDate';
        $counts = $query
            ->select(
                DB::raw('count(*) as Total'),
                'Department',
                'ClassificationID'
            )
            ->where($dateField, '>=', DB::raw('DATEADD(MONTH, -12, GETDATE())'))
            ->groupBy('Department', 'ClassificationID')
            ->get();
        $classifications = Classification::pluck('Classification', 'ClassID');

        /*
        labels = [x1, x2, ...]; => departments
        datasets = [
            {y1, [x1_value, x2_value, ...]}
            {y2, [x1_value, x2_value, ...]}
            {y3, [x1_value, x2_value, ...]}
        ]
        => y1, y2, y3 => Classifications
        */
        $chartDatasets = [];
        $total = 0;
        foreach ($classifications as $classId => $className) {
            if ($type == self::IMP_TYPE) {
                $className = str_ireplace('Initiative', 'Improvement', $className);
            }

            $dataset = ['label' => $className];
            $data = array_fill(0, count($departments), 0);
            foreach ($counts as $count) {
                if ($count->ClassificationID == $classId) {
                    foreach ($departments as $index => $department) {
                        $value = $count->Department == $department ? (integer) $count->Total : 0;
                        $data[$index] += $value;
                        $total += $value;
                    }
                }
            }
            $dataset['data'] = $data;
            $chartDatasets[] = $dataset;
        }
        return [
            'labels' => $departments,
            'datasets' => $chartDatasets,
            'total' => $total
        ];
    }

    public function initForDept() {
        $user = Auth::user();

        if ($user->isCEO()) {
            return $this->countsPerDirectorate(self::INIT_TYPE);
        } elseif ($user->hasNoDepartment()) {
            return $this->countsPerDepartmentOfDirectorate(self::INIT_TYPE);
        }

        return $this->countsPerUnitOfDept(self::INIT_TYPE);
    }

    public function impForDept() {
        $user = Auth::user();

        if (Auth::user()->isCEO()) {
            return $this->countsPerDirectorate(self::IMP_TYPE);
        } elseif ($user->hasNoDepartment()) {
            return $this->countsPerDepartmentOfDirectorate(self::IMP_TYPE);
        }

        return $this->countsPerUnitOfDept(self::IMP_TYPE);
    }

    private function countsBenefits($type)
    {
        $query = $type == self::INIT_TYPE ? $this->getInitQuery() : $this->getImpQuery();
        $records = $query->join('Init_Benefits', 'InitID', '=', 'Initiatives.ID')
            ->get();
        $labels = ['Customer', 'Quality', 'People', 'Enviroment', 'Risk', 'Economic'];
        $counts = [0, 0, 0, 0, 0, 0];
        foreach ($records as $record) {
            $counts[0] += ($record->Service ? 1 : 0);
            $counts[1] += ($record->Risk ? 1 : 0);
            $counts[2] += ($record->Quality ? 1 : 0);
            $counts[3] += ($record->People ? 1 : 0);
            $counts[4] += ($record->Enviro ? 1 : 0);
            $counts[5] += ($record->Eco ? 1 : 0);
        }
        return [
            'labels' => $labels,
            'values' => $counts
        ];
    }

    public function countsBenefitsForInit()
    {
        return $this->countsBenefits(self::INIT_TYPE);
    }

    public function countsBenefitsForImp()
    {
        return $this->countsBenefits(self::IMP_TYPE);
    }
}
