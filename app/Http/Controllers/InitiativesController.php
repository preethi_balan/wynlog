<?php

namespace App\Http\Controllers;

use App\Models\Initiatives;
use App\Models\InitBenefits;
use App\Models\InitReportingTags;
use App\Models\InitOtherDept;
use App\Models\InitSupportDept;
use App\Models\Phase;
use App\Models\Classification;
use App\Models\AD;
use App\Models\InitUserAccess;
use App\Models\InitComments;
use App\Models\InitReferences;
use App\Models\ServiceCatalogue;
use App\Providers\ModelLoggingServiceProvider;
use Auth;
use Mail;
use Request;
use DB;
use Gate;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class InitiativesController extends Controller
{
    private static $UPLOAD_DIR;

    public function __construct() {
        parent::__construct();
        static::$UPLOAD_DIR = storage_path('app/references');
    }

    public function initiatives()
    {
        $initiatives = Initiatives::withRelatedData()
            ->listInitsByUserType()
            ->orderBy('DateCreated', 'DESC')
            ->get();

        // List of Classification, Phase, used for filter
        $classifications = Classification::all();
        $phases = Phase::where('Phase', '!=', 'Completed')->get();

        return view('initiatives.index')->with([
            'initiatives' => $initiatives,
            'classifications' => $classifications,
            'phases' => $phases,
        ]);
    }

    public function improvements()
    {
        $improvements = Initiatives::withRelatedData()
            ->selectImprovements()
            ->orderBy('CompleteDate', 'DESC')
            ->get();

        // List of Classification, Phase, used for filter
        $classifications = Classification::all();

        return view('improvements.index')->with([
            'improvements' => $improvements,
            'classifications' => $classifications,
        ]);
    }

    public function viewDetails($initID)
    {
        if (!static::isInteger($initID)) {
            abort(404);
        }
        $model = Initiatives::find($initID);
        if (!$model) {
            abort(404);
        }
        if (Gate::denies('view-initiative', $model)) {
            abort(404);
        }

        $print = Request::input('print', false);
        $viewName = ($print ? 'initiatives.print' : 'initiatives.view');

        // Log Views
        ModelLoggingServiceProvider::logViews($model);

        return view($viewName)->with([
            'model' => $model,
        ]);
    }

    public function createOrUpdate($initID = null)
    {
        if ($initID) {
            if (!static::isInteger($initID)) {
                return redirect()->to('create-initiative');
            }

            $model = Initiatives::find($initID);
            if (!$model) {
                session()->flash('error_message', 'Initiative doesn\'t exist. Do you want to create new Initiative?');
                return redirect()->to('create-initiative');
            }
            if (Gate::denies('edit-initiative', $model)) {
                abort(404);
            }

            // Log Views
            ModelLoggingServiceProvider::logViews($model);
        } else {
            $model = new Initiatives();
            $model->InitBenefits = new InitBenefits();
        }

        $directorates = AD::select('directorate')
            ->whereNotNull('directorate')
            ->distinct()
            ->get();
        // List of service id, used to display in the Service ID form element
        // $services = ServiceCatalogue::all();

        $classifications = Classification::all();

        $users = AD::all();

        $phases = Phase::all();

        return view('initiatives.create-or-update')->with([
            'model' => $model,
            'directorates' => $directorates,
            'phases' => $phases,
            // 'services' => $services,
            'departments' => AD::select('department')->whereNotNull('department')->distinct()->pluck('department'),
            'supportingDepartments' => $model->InitSupportDept->pluck('Department')->toArray(),
            'classifications' => $classifications,
            'users' => $users
        ]);
    }

    public function save($initID = null)
    {
        $this->validate(request(), [
            'directorate' => 'required',
            // 'department' => 'required',
            // 'business-unit' => 'required',
            // 'service-id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'classification-id' => 'required',
            'cross-department' => 'required_if:classification-id,' . Classification::CROSS_ORGANISATIONAL_INITIATIVE,
            //'stakeholders' => 'required',
            'project-sponsor' => 'required',
            'actioning-officer' => 'required',
            'start-date' => 'required',
            'service-benefit-details' => 'required_if:service-benefit,1',
            'risk-benefit-details' => 'required_if:risk-benefit,1',
            'quality-benefit-details' => 'required_if:quality-benefit,1',
            'people-benefit-details' => 'required_if:people-benefit,1',
            'environmental-benefit-details' => 'required_if:environmental-benefit,1',
            'economic-benefit-details' => 'required_if:economic-benefit,1',
            'ref-files.*' => 'max-file-size',
            'completion-date' => 'required_if:phase-id,' . Phase::COMPLETED,
            'comments' => 'required_if:phase-id,' . Phase::CANCELLED . ',' . Phase::ONHOLD,
        ]);
        if ($initID) {
            if (!static::isInteger($initID)) {
                return response()->json(['error_message' => 'Request failed!'], 400);
            }

            $model = Initiatives::find($initID);
            if (!$model) {
                return response()->json(['error_message' => 'Initiative doesn\'t exist.'], 400);
            }
            if (Gate::denies('edit-initiative', $model)) {
                return response()->json(['error_message' => 'Cannot edit this Initiative or Improvement.'], 400);
            }
        } else {
            $model = new Initiatives();
        }

        $isNewModel = !$model->exists;

        if ($isNewModel) {
            $model->Username = Auth::user()->username;
            $model->Name = Auth::user()->name;
        }
        $model->Directorate = Request::input('directorate');
        $model->Department = Request::input('department');
        $model->BusinessUnit = Request::input('business-unit');
        $model->ServiceID = null; // Request::input('service-id');
        $model->Title = Request::input('title');
        $model->Description = Request::input('description');
        $model->ClassificationID = Request::input('classification-id');
        $model->Stakeholders = Request::input('stakeholders');
        $model->Sponsor = Request::input('project-sponsor');
        $model->ActioningOfficer = Request::input('actioning-officer');
        $model->StartDate = Request::input('start-date');
        if ($isNewModel) {
            $model->PhaseID = Phase::PHASE_NEW;
        } else {
            $phaseID = Request::input('phase-id');
            $model->PhaseID = $phaseID;
            if ($phaseID == Phase::COMPLETED) {
                $model->CompleteDate = Request::input('completion-date');

                // Validate at least one benefit
                if (Request::input('service-benefit') != 1
                    && $riskBenefit = Request::input('risk-benefit') != 1
                    && $qualityBenefit = Request::input('quality-benefit') != 1
                    && $peopleBenefit = Request::input('people-benefit') != 1
                    && $environmentalBenefit = Request::input('environmental-benefit') != 1
                    && $economicBenefit = Request::input('economic-benefit') != 1) {
                    return response()->json(['error_message' => 'Please add at least one Benefit for Improvement!'], 400);
                }

            } else {
                $model->CompleteDate = null;
            }
        }
        $confidential = Request::input('confidential', '0');
        $isChangedToConfidential = $confidential == 1 && $model->Confidential != $confidential;
        $model->Confidential = $confidential;

        $isDirty = $model->isDirty();

        DB::beginTransaction();
        try {
            if ($model->save()) {
                // List all departments, include Initiative Department, Collaborating/Supporting Departments
                $allDepartments = [$model->Department];

                // InitOtherDept
                if ($model->ClassificationID == Classification::CROSS_ORGANISATIONAL_INITIATIVE) {
                    // Old departments
                    $initOtherDepts = [];
                    $initOtherDeptsModels = $model->InitOtherDept;
                    foreach ($initOtherDeptsModels as $department) {
                        $initOtherDepts[] = $department->Department;
                    }
                    // New departments
                    $initOtherDeptsNew = Request::input('cross-department', []);
                    $initOtherDeptsNewModels = [];
                    foreach ($initOtherDeptsNew as $department) {
                        if (!in_array($department, $initOtherDepts)) {
                            $initOtherDeptsNewModels[] = InitOtherDept::create([
                                'InitID' => $model->ID,
                                'Department' => $department
                            ]);
                        }

                        $allDepartments[] = $department;
                    }
                    // Delete old departments which not exist in new list
                    $deptIdsToDelete = [];
                    foreach ($initOtherDeptsModels as $dept) {
                        if (!in_array($dept->Department, $initOtherDeptsNew)) {
                            $deptIdsToDelete[] = $dept->ID;
                        }
                    }
                    if (count($deptIdsToDelete)) {
                        InitOtherDept::destroy($deptIdsToDelete);
                    }
                } else {
                    $model->InitOtherDept->each(function($item) {
                        $item->delete();
                    });
                }

                // InitSupportDept
                // Old departments
                $initSupportDeptsModels = $model->InitSupportDept;
                $initSupportDepts = $initSupportDeptsModels->pluck('Department')->toArray();;
                // New departments
                $initSupportDeptsNew = Request::input('supporting-departments', []);
                $initSupportDeptsNewModels = [];
                foreach ($initSupportDeptsNew as $department) {
                    if (!in_array($department, $initSupportDepts)) {
                        $initSupportDeptsNewModels[] = InitSupportDept::create([
                            'InitID' => $model->ID,
                            'Department' => $department
                        ]);
                    }

                    $allDepartments[] = $department;
                }
                // Delete old departments which not exist in new list
                $deptIdsToDelete = [];
                foreach ($initSupportDeptsModels as $dept) {
                    if (!in_array($dept->Department, $initSupportDeptsNew)) {
                        $deptIdsToDelete[] = $dept->ID;
                    }
                }
                if (count($deptIdsToDelete)) {
                    InitSupportDept::destroy($deptIdsToDelete);
                }

                /*
                 InitUserAccess
                */
                // Line of direct reports of Actioning Officer
                $positionNos = $model->ActioningOfficerUser->getPositions();

                // Manager of any departments listed under "Department"
                // as well as "Collaborating/Supporting Departments"
                // And Relevant director of departments above
                $departUsers = AD::whereIn('department', $allDepartments)->get();
                foreach ($departUsers as $user) {
                    if ($user->isManagerOrDirector()) {
                        $positionNos[] = $user->position;
                    }
                }

                // In addition to Actioning Officer’s line of direct reports,
                // could we give the same access to the coordinator/manager/director
                // of the Business Unit/Department/Directorate selected
                if ($model->BusinessUnit) {
                    $accessUser = AD::where('group', $model->BusinessUnit)->first();
                    if ($accessUser) {
                        $positionNos = array_merge($positionNos, $accessUser->getPositions());
                    }
                }
                if ($model->Department) {
                    $accessUser = AD::where('department', $model->Department)->first();
                    if ($accessUser) {
                        if ($accessUser->managerPosition) {
                            $positionNos[] = $accessUser->managerPosition;
                        }
                        if ($accessUser->directorPosition) {
                            $positionNos[] = $accessUser->directorPosition;
                        }
                    }
                }
                if ($model->Directorate) {
                    $accessUser = AD::where('directorate', $model->Directorate)->first();
                    if ($accessUser && $accessUser->directorPosition) {
                        $positionNos[] = $accessUser->directorPosition;
                    }
                }

                $positionNos = array_unique($positionNos);

                $newInitUserAccesses = [];
                if ($isNewModel) {
                    foreach ($positionNos as $positionNo) {
                        $newInitUserAccesses[] = InitUserAccess::create([
                            'InitID' => $model->ID,
                            'PositionNo' => $positionNo
                        ]);
                    }
                } else {
                    $initUserAccesses = $model->InitUserAccess;
                    // Old user access positions
                    $oldPositionNos = [];
                    $oldPositionNosToDelete = [];
                    foreach ($initUserAccesses as $access) {
                        $oldPositionNos[] = $access->PositionNo;
                        // Delete old user access if it not in list new user access
                        if (! in_array($access->PositionNo, $positionNos)) {
                            $oldPositionNosToDelete[] = $access->ID;
                        }
                    }
                    if (count($oldPositionNosToDelete)) {
                        InitUserAccess::destroy($oldPositionNosToDelete);
                    }
                    // New user access
                    foreach ($positionNos as $positionNo) {
                        if (! in_array($positionNo, $oldPositionNos)) {
                            $newInitUserAccesses[] = InitUserAccess::create([
                                'InitID' => $model->ID,
                                'PositionNo' => $positionNo
                            ]);
                        }
                    }
                }

                // InitReferences
                $refComments = Request::input('ref-comments', []);
                $refCommentsDesc = Request::input('ref-comments-desc', []);
                if ($refComments) {
                    $refCommentsModels = [];
                    foreach ($refComments as $index => $comment) {
                        $refCommentsModels[] = InitReferences::create([
                            'InitID' => $model->ID,
                            'Type' => InitReferences::TYPE_TEXT,
                            'Content' => $comment,
                            'Description' => $refCommentsDesc[$index] ?: '',
                        ]);
                    }
                }
                $refUrls = Request::input('ref-urls', []);
                $refUrlsDesc = Request::input('ref-urls-desc', []);
                if ($refUrls) {
                    $refUrlsModels = [];
                    foreach ($refUrls as $index => $url) {
                        $refUrlsModels[] = InitReferences::create([
                            'InitID' => $model->ID,
                            'Type' => InitReferences::TYPE_URL,
                            'Content' => $url,
                            'Description' => $refUrlsDesc[$index] ?: '',
                        ]);
                    }
                }
                $refFiles = Request::file('ref-files', []);
                $refFilesDesc = Request::input('ref-files-desc', []);
                if ($refFiles) {
                    $refFilesModels = [];
                    foreach ($refFiles as $index => $file) {
                        $filename = $model->ID . '_' . time() . '_' . Auth::user()->username . '_' . $file->getClientOriginalName();

                        $uploadSuccess = $file->move(static::$UPLOAD_DIR, $filename);
                        if ($uploadSuccess) {
                            $refFilesModels[] = InitReferences::create([
                                'InitID' => $model->ID,
                                'Type' => InitReferences::TYPE_FILE,
                                'Content' => $filename,
                                'Description' => $refFilesDesc[$index] ?: '',
                            ]);
                        }
                    }
                }

                // Init Reporting tags
                if (!$isNewModel) {
                    $initReportingTags = $model->InitReportingTags;
                }
                if ($isNewModel || !$initReportingTags) {
                    $initReportingTags = new InitReportingTags();
                    $initReportingTags->InitID = $model->ID;
                }
                $tagColumns = InitReportingTags::columns();
                foreach ($tagColumns as $column) {
                    $columnName = $column['column'];
                    if (Request::has($columnName) && Request::input($columnName) == 1) {
                        $initReportingTags->{$columnName} = 1;
                    } else {
                        $initReportingTags->{$columnName} = 0;
                    }
                }
                $initReportingTags->save();
                if ($isNewModel) {
                    ModelLoggingServiceProvider::logInitReportingTags($model->InitReportingTags, ModelLoggingServiceProvider::ACTION_CREATE);
                }

                // InitBenefits
                $serviceBenefit = Request::input('service-benefit');
                $riskBenefit = Request::input('risk-benefit');
                $qualityBenefit = Request::input('quality-benefit');
                $peopleBenefit = Request::input('people-benefit');
                $environmentalBenefit = Request::input('environmental-benefit');
                $economicBenefit = Request::input('economic-benefit');

                if ($isNewModel) {
                    $initBenefits = new InitBenefits();
                    $initBenefits->InitID = $model->ID;
                } else {
                    $initBenefits = $model->InitBenefits;
                }
                $initBenefits->Service = $serviceBenefit;
                $initBenefits->ServiceDetails = $serviceBenefit == 1 ? Request::input('service-benefit-details') : null;
                $initBenefits->Risk = $riskBenefit;
                $initBenefits->RiskDetails = $riskBenefit == 1 ? Request::input('risk-benefit-details') : null;
                $initBenefits->Quality = $qualityBenefit;
                $initBenefits->QualityDetails = $qualityBenefit == 1 ? Request::input('quality-benefit-details') : null;
                $initBenefits->People = $peopleBenefit;
                $initBenefits->PeopleDetails = $peopleBenefit == 1 ? Request::input('people-benefit-details') : null;
                $initBenefits->Enviro = $environmentalBenefit;
                $initBenefits->EnviroDetails = $environmentalBenefit == 1 ? Request::input('environmental-benefit-details') : null;
                $initBenefits->Eco = $economicBenefit;
                $initBenefits->EcoDetails = $economicBenefit == 1 ? Request::input('economic-benefit-details') : null;
                $initBenefits->save();
                if ($isNewModel) {
                    ModelLoggingServiceProvider::logInitBenefits($model->InitBenefits, ModelLoggingServiceProvider::ACTION_CREATE);
                }

                $logInitAction = $isNewModel ? ModelLoggingServiceProvider::ACTION_CREATE : ModelLoggingServiceProvider::ACTION_UPDATE;

                // Comments
                $comments = Request::input('comments', '');
                if ($comments) {
                    $model->InitComments()->save(InitComments::create([
                       'Comments' => $comments,
                       'Username' => Auth::user()->username,
                       'InitID' => $model->ID,
                       'PhaseID' => $model->PhaseID
                    ]));

                    $logInitAction = ModelLoggingServiceProvider::ACTION_INIT_COMMENT . $comments;
                    $isDirty = true;
                }

                DB::commit();

                if ($isNewModel || $isDirty) {
                    // Log Initiative
                    try {
                        ModelLoggingServiceProvider::logInitiative($model, $logInitAction);
                    } catch (\Exception $le) {
                        Log::error('Error when logging initiative: ');
                        Log::error($e);
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json(['error_message' => 'Error occurred! Please try again...'], 400);
        }

        /*
        Send emails after commit to db
        */
        try {
            // Send email to user and Actioning Officer
            if ($isNewModel) {
                Mail::send('emails.created', ['initiative' => $model], function ($m) use ($model) {
                    $m->from(env('MAIL_USERNAME'), 'Wyn-Log');
                    $m->subject('New Initiative #' . $model->ID);

                    // User submitting
                    $m->to(Auth::user()->email);

                    // Actioning Officer
                    $actioningOfficer = $model->ActioningOfficerUser;
                    if ($actioningOfficer && $actioningOfficer->email) {
                        $m->cc($actioningOfficer->email);
                    }
                });
            }

            if ($isChangedToConfidential) {
                Mail::send('emails.confidential', ['initiative' => $model], function ($m) use ($model) {
                    $m->from(env('MAIL_USERNAME'), 'Wyn-Log');

                    $m->to(env('MAIL_TO_CONFIDENTIAL'))->subject('New Confidential Initiative #' . $model->ID);
                });
            }
        } catch (\Exception $e) {
            Log::error('Error in send mails: ' . $e->getMessage());
            Log::error($e);
        }

        return response()->json(['success' => true], 200);
    }

    public function getAllDepartments()
    {
        return AD::select('department')
            ->whereNotNull('department')
            ->distinct()
            ->get()
            ->toJson();
    }

    public function getDepartmentsOfDirectorate()
    {
        $directorate = Request::input('directorate');
        if (!$directorate) {
            return response()->json([]);
        }
        return AD::select('department')
            ->where('directorate', '=', $directorate)
            ->whereNotNull('department')
            ->distinct()
            ->get()
            ->toJson();
    }

    public function getBusinessUnitsOfDepartment()
    {
        $department = Request::input('department');
        if (!$department) {
            return response()->json([]);
        }
        return AD::select('group')
            ->where('department', '=', $department)
            ->whereNotNull('group')
            ->distinct()
            ->get()
            ->toJson();
    }

    public function getReferenceFile($filename)
    {
        $filepath = static::$UPLOAD_DIR . DIRECTORY_SEPARATOR . $filename;
        if (is_file($filepath)) {
            return response()->file($filepath);
        }
        abort(404);
    }

    public function removeReference()
    {
        $refID = Request::input('ID');
        $ref = InitReferences::find($refID);
        if (!$ref || !$ref->exists) {
            return response()->json(['error' => '1', 'message' => 'Reference not found!'], 400);
        }

        $init = Initiatives::find($ref->InitID);
        // Ref of Improvement cannot be deleted
        if (Gate::denies('edit-initiative', $init)) {
            return response()->json(['error' => '1', 'message' => 'Cannot delete this reference!'], 400);
        }

        if ($ref->delete()) {
            if ($ref->Type == InitReferences::TYPE_FILE) {
                $filepath = static::$UPLOAD_DIR . DIRECTORY_SEPARATOR . $ref->Content;
                if (is_file($filepath)) {
                    unlink($filepath);
                }
            }
            return response()->json(['error' => '0', 'message' => 'Deleted successfully!'], 200);
        }
        return response()->json(['error' => '1', 'message' => 'Internal error!'], 500);
    }
}
