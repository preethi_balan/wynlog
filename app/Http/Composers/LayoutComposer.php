<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\AD;
use App\Models\Initiatives;
use Auth;
use DB;

class LayoutComposer {
    private $currentYear;

    public function __construct()
    {
        $this->currentYear = date('Y');
    }

    private function initQuery()
    {
        return Initiatives::selectInitiatives()
            ->where(DB::raw('year(LastUpdated)'), $this->currentYear);
    }

    private function impQuery()
    {
        return Initiatives::selectImprovements()
            ->where(DB::raw('year(LastUpdated)'), $this->currentYear);
    }

    public function compose(View $view)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $group = $user->group;
            $businessUnitStats = [
                'init' => $group ? $this->initQuery()->where('BusinessUnit', $group)->count() : 0,
                'imp' => $group ? $this->impQuery()->where('BusinessUnit', $group)->count() : 0,
            ];

            $dept = $user->department;
            $departmentStats = [
                'init' => $dept ? $this->initQuery()->where('Department', $dept)->count() : 0,
                'imp' => $dept ? $this->impQuery()->where('Department', $dept)->count() : 0,
            ];

            $directorate = $user->directorate;
            $directorateStats = [
                'init' => $directorate ? $this->initQuery()->where('Directorate', $directorate)->count() : 0,
                'imp' => $directorate ? $this->impQuery()->where('Directorate', $directorate)->count() : 0,
            ];

            $view->with([
                'user' => $user,
                'currentYear' => $this->currentYear,
                'businessUnitStats' => $businessUnitStats,
                'departmentStats' => $departmentStats,
                'directorateStats' => $directorateStats,
            ]);
        }
    }

}