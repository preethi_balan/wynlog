<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// See vendor/laravel/framework/src/Illuminate/Routing/Router.php
// Authentication Routes...
// Route::get('login', 'Auth\AuthController@showLoginForm');
// Route::post('login', 'Auth\AuthController@authenticate');
// Route::get('logout', 'Auth\AuthController@logout');

Route::get('impersonate', 'UserController@showImpersonateForm');
Route::post('impersonate', 'UserController@impersonate');
Route::get('impersonate/stop', 'UserController@stopImpersonate');

Route::get('initiatives', "InitiativesController@initiatives");
Route::get('improvements', "InitiativesController@improvements");
Route::get('create-initiative', "InitiativesController@createOrUpdate");
Route::post('create-initiative', "InitiativesController@save");
Route::get('edit-initiative/{id}', "InitiativesController@createOrUpdate");
Route::post('edit-initiative/{id}', "InitiativesController@save");
Route::post('list-departments', "InitiativesController@getDepartmentsOfDirectorate");
Route::post('list-all-departments', "InitiativesController@getAllDepartments");
Route::post('list-business-units', "InitiativesController@getBusinessUnitsOfDepartment");
Route::get('reference-file/{filename}', "InitiativesController@getReferenceFile");
Route::post('remove-reference', "InitiativesController@removeReference");
Route::get('view-initiative/{id}', "InitiativesController@viewDetails");
Route::get('view-improvement/{id}', "InitiativesController@viewDetails");
Route::get('about', "AboutController@index");

Route::get('/', "DashboardController@index");
Route::group(['prefix' => 'dashboard'], function () {
    Route::get('init-counts-pies', "DashboardController@initCounts");
    Route::get('init-counts-months-line', "DashboardController@initCountsBy12Months");
    Route::get('imp-counts-months-line', "DashboardController@impCountsBy12Months");
    Route::get('init-for-dept-stacked-bar', "DashboardController@initForDept");
    Route::get('imp-for-dept-stacked-bar', "DashboardController@impForDept");
    Route::get('init-counts-benefits-hbar', "DashboardController@countsBenefitsForInit");
    Route::get('imp-counts-benefits-hbar', "DashboardController@countsBenefitsForImp");
});
