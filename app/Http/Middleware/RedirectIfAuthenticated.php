<?php

namespace App\Http\Middleware;

use App\Models\AD;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $message = 'Windows Authentication fail.';

        if (!$request->server('AUTH_USER')) {
            return response($message, 401);
        }

        // Allow users to get through only if they are logged in via windows auth
        if (Auth::guard($guard)->guest()) {
            $authUser = $request->server('AUTH_USER');
            $authUserArray = explode('\\', $authUser);

            if ($authUserArray && isset($authUserArray[1])) {
                $username = $authUserArray[1];
                $user = AD::where('username', '=', $username)->first();

                if ($user) {
                    Auth::guard($guard)->login($user);

                    return $next($request);
                }
            }

            return response($message, 401);
        }

        return $next($request);
    }
}
