<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Support\Facades\Redirect;

class LowerCaseRoutes
{
    /**
     * IMPORTANT: all Laravel routes must be defined as lowercase
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentPath = $request->path();
        if(!ctype_lower(preg_replace('/[^A-Za-z]/', '', $currentPath))
            && $currentPath !== "/") {
            $new_route = str_replace($currentPath, strtolower($currentPath), $request->fullUrl());

            return Redirect::to($new_route, 301);
        }

        return $next($request);
    }
}
