<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Impersonate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->is('impersonate')) {
            if ($user = Auth::guard($guard)->user()) {
                if (!$user || !$user->isAdmin()) {
                    return redirect('/');
                }
            }
        }

        if ($request->session()->has('impersonate')) {
            Auth::onceUsingId($request->session()->get('impersonate'));
        }

        return $next($request);
    }
}
