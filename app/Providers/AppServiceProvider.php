<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Validation\ValidatorExtended;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->validator->resolver(function($translator, $data, $rules, $messages = array(), $customAttributes = array()) {
            return new ValidatorExtended($translator, $data, $rules, $messages, $customAttributes);
        });

        View::composer(
            'layouts.app', 'App\Http\Composers\LayoutComposer'
        );
        View::composer(
            'dashboard.index', 'App\Http\Composers\LayoutComposer'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
