<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models;
use Auth;

class ModelLoggingServiceProvider extends ServiceProvider
{
    const ACTION_CREATE = 'Create';
    const ACTION_UPDATE = 'Update';
    const ACTION_DELETE = 'Delete';
    const ACTION_INIT_COMMENT = 'Comment: ';

    public static function logInitiative($init, $action)
    {
        $logInitiative = new Models\LogInitiatives();
        $logInitiative->InitID = $init->ID;
        $logInitiative->Username = $init->Username;
        $logInitiative->Name = $init->Name;
        $logInitiative->Directorate = $init->Directorate;
        $logInitiative->Department = $init->Department;
        $logInitiative->BusinessUnit = $init->BusinessUnit;
        $logInitiative->ServiceID = $init->ServiceID;
        $logInitiative->Title = $init->Title;
        $logInitiative->Description = $init->Description;
        $logInitiative->ClassificationID = $init->ClassificationID;
        $logInitiative->Stakeholders = $init->Stakeholders;
        $logInitiative->Sponsor = $init->Sponsor;
        $logInitiative->ActioningOfficer = $init->ActioningOfficer;
        $logInitiative->StartDate = $init->StartDate;
        $logInitiative->PhaseID = $init->PhaseID;
        $logInitiative->CompleteDate = $init->CompleteDate;
        $logInitiative->Confidential = $init->Confidential;
        $logInitiative->LogAction = $action;
        $logInitiative->LogActionedBy = Auth::user()->username;
        $logInitiative->save();
    }

    public static function logInitBenefits($initBenefit, $action)
    {
        $logInitBenefit = new Models\LogInitBenefits;
        $logInitBenefit->InitID = $initBenefit->InitID;
        $logInitBenefit->Service = $initBenefit->Service;
        $logInitBenefit->ServiceDetails = $initBenefit->ServiceDetails;
        $logInitBenefit->Risk = $initBenefit->Risk;
        $logInitBenefit->RiskDetails = $initBenefit->RiskDetails;
        $logInitBenefit->Quality = $initBenefit->Quality;
        $logInitBenefit->QualityDetails = $initBenefit->QualityDetails;
        $logInitBenefit->People = $initBenefit->People;
        $logInitBenefit->PeopleDetails = $initBenefit->PeopleDetails;
        $logInitBenefit->Enviro = $initBenefit->Enviro;
        $logInitBenefit->EnviroDetails = $initBenefit->EnviroDetails;
        $logInitBenefit->Eco = $initBenefit->Eco;
        $logInitBenefit->EcoDetails = $initBenefit->EcoDetails;
        $logInitBenefit->LogAction = $action;
        $logInitBenefit->LogActionedBy = Auth::user()->username;
        $logInitBenefit->save();
    }

    public static function logInitReportingTags($initReportingTags, $action)
    {
        $logInitReportingTags = new Models\LogInitReportingTags;
        $logInitReportingTags->InitID = $initReportingTags->InitID;

        $columns = Models\InitReportingTags::columns();
        foreach ($columns as $column) {
            $logInitReportingTags->{$column['column']} = $initReportingTags->{$column['column']};
        }

        $logInitReportingTags->LogAction = $action;
        $logInitReportingTags->LogActionedBy = Auth::user()->username;
        $logInitReportingTags->save();
    }

    public static function logInitOtherDept($initOtherDept, $action)
    {
        $logInitOtherDept = new Models\LogInitOtherDept;
        $logInitOtherDept->RefID = $initOtherDept->ID;
        $logInitOtherDept->InitID = $initOtherDept->InitID;
        $logInitOtherDept->Department = $initOtherDept->Department;
        $logInitOtherDept->LogAction = $action;
        $logInitOtherDept->LogActionedBy = Auth::user()->username;
        $logInitOtherDept->save();
    }

    public static function logInitSupportDept($initSupportDept, $action)
    {
        $logInitSupportDept = new Models\LogInitSupportDept;
        $logInitSupportDept->RefID = $initSupportDept->ID;
        $logInitSupportDept->InitID = $initSupportDept->InitID;
        $logInitSupportDept->Department = $initSupportDept->Department;
        $logInitSupportDept->LogAction = $action;
        $logInitSupportDept->LogActionedBy = Auth::user()->username;
        $logInitSupportDept->save();
    }

    public static function logViews($init)
    {
        $logViews = new Models\LogViews;
        $logViews->InitID = $init->ID;
        $logViews->ViewedBy = Auth::user()->username;
        $logViews->save();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Models\InitBenefits::updated(function ($initBenefit) {
            ModelLoggingServiceProvider::logInitBenefits($initBenefit, self::ACTION_UPDATE);
        });

        Models\InitReportingTags::updated(function ($initReportingTags) {
            ModelLoggingServiceProvider::logInitReportingTags($initReportingTags, self::ACTION_UPDATE);
        });

        Models\InitOtherDept::created(function ($initOtherDept) {
            ModelLoggingServiceProvider::logInitOtherDept($initOtherDept, self::ACTION_CREATE);
        });

        Models\InitOtherDept::deleted(function ($initOtherDept) {
            ModelLoggingServiceProvider::logInitOtherDept($initOtherDept, self::ACTION_DELETE);
        });

        Models\InitSupportDept::created(function ($initSupportDept) {
            ModelLoggingServiceProvider::logInitSupportDept($initSupportDept, self::ACTION_CREATE);
        });

        Models\InitSupportDept::deleted(function ($initSupportDept) {
            ModelLoggingServiceProvider::logInitSupportDept($initSupportDept, self::ACTION_DELETE);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
