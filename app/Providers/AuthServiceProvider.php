<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\AD;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        /*
        Admin has all abilities
        */
        $gate->before(function ($user, $ability) {
            if ($user->isAdmin() || $user->isCEO()) {
                return true;
            }
        });

        /*
        Check if user can view initiative
        */
        $gate->define('view-initiative', function ($user, $initiative) {
            if ($user->username == $initiative->Username
                || $user->username == $initiative->ActioningOfficer) {
                return true;
            }

            if ($initiative->Confidential) {
                // Check if user can view an confidential initiative
                // => users with position in Init_UserAccess
                if (!$initiative->InitUserAccess) {
                    return false;
                }

                return in_array($user->position, $initiative->InitUserAccess->pluck('PositionNo')->toArray());
            }

            // Normal initiative => user with department in Initiative, InitOtherDept
            // Improvement => all
            $userDeparts = $user->department ? [$user->department] : [];
            if ($user->hasNoDepartment()) {
                $directorateDepts = AD::select('department')
                    ->where('directorate', '=', $user->directorate)
                    ->whereNotNull('department')
                    ->distinct()
                    ->pluck('department')
                    ->toArray();
                $userDeparts = array_merge($userDeparts, $directorateDepts);
            }

            if ($initiative->isImprovement()
                || in_array($initiative->Department, $userDeparts)
                || $initiative->InitOtherDept()->whereIn('Department', $userDeparts)->count() > 0
                || $initiative->InitSupportDept()->whereIn('Department', $userDeparts)->count() > 0) {
                return true;
            }

            return false;
        });

        /*
        Check if user can edit an initiative => users with position in positions of user who created init and actioning officer user
        */
        $gate->define('edit-initiative', function ($user, $initiative) {
            // Cannot edit improvement
            if ($initiative->isImprovement()) {
                return false;
            }

            if ($user->username == $initiative->Username
                || $user->username == $initiative->ActioningOfficer) {
                return true;
            }

            $initiative->InitUserAccess;
            if (!$initiative->InitUserAccess) {
                return false;
            }

            return in_array($user->position, $initiative->InitUserAccess->pluck('PositionNo')->toArray());
        });
    }
}
